<?php

// KEMENKEU SOA HubPayment Bridging
// KEMENKEU SIMPONI BRI
// DEPHUB-JTN-TELKOM
// Andri Ari Pratama
// me@aap.my.id
// 2015

require 'app/init.php';

$app->get('/', function() use ($app,$mssql) {
  $app->response->headers->set('Content-Type', 'application/json');

});

$app->get('/PaymentInput/:data+', function ($name) use ($app){
  $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");

  // $app->response->headers->set('Content-Type', 'application/json');
  $timeRequest = time();
  $volume = $name[5];
  $satuan = $name[6];
  $grandTotal = $name[4] * $name[5];
  $payment = array(
    // Data for Application
  	"appsId" => "009",
  	"invoiceNo" => $name[0] . "/OPS/DKUPPU-PPO/".date("m")."/".date("Y"),
  	"routeId" => "001",
  	"data" => array(
        "PaymentHeader" => array(
          "TrxId" => "ANY-JK",
          "UserId" => "0",
        	"Password" => "0",
          "ExpiredDate" => date("Y-m-d H:i:s", time()+86400),
        	"DateSent" => date("Y-m-d H:i:s", time()),
        	"KodeKL" => "022",
        	"KodeEselon1" => "05",
        	"KodeSatker" => "465632",
          "JenisPNBP" => "F",
          "KodeMataUang" => "1",
          "TotalNominalBilling" => $name[1],
          "NamaWajibBayar" => $name[2],
        ),
        // PaymentDetails - Data for all transaction
        "PaymentDetails" => array(
          // PaymentDetail - Place all data how many user transcation ( Bulk Input )
          "PaymentDetail" => array(
            "NamaWajibBayar" => $name[2],
            "KodeTarifSimponi" => $name[3],
            "KodePPSimponi" => "2015011",
            "KodeAkun" => "423216",
            "TarifPNBP" => $name[4],
            "Volume" => $volume,
            "Satuan" => $satuan,
            "TotalTarifPerRecord" => $grandTotal,
          ),
        ),
      ),

  	);
  $paymentJson = json_encode($payment);
  $res = $client->PaymentRequest($payment);
  $res = json_encode($res);
  // echo $res;
  $data = json_decode($res, true);
  if($data["response"]["code"] == 00) {
    $log = $app->db->table('Trx_Record')->insertGetId([
      'req_time' => time(), 'req_entity' => $paymentJson, 'req_response' => $res, 'req_billingcode' => $data["response"]["simponiData"]["KodeBillingSimponi"], 'req_expired' => $data["response"]["simponiData"]["ExpiredDate"]
    ]);
    $params = array('KodeBillingSimponi' => $data["response"]["simponiData"]["KodeBillingSimponi"], 'GrandTotal' => $name[1]);
    $app->render('billingcode.html', $params);
    // echo "Your Billing Code / Kode Billing Anda : ".$data["response"]["simponiData"]["KodeBillingSimponi"];
  } else {

  }
  // echo $data["response"]["message"];

});
$app->get('/BillingStatus/:kodebilling', function($billingcode) use ($app) {
  $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl", array('cache_wsdl' => WSDL_CACHE_NONE, 'features'=>SOAP_SINGLE_ELEMENT_ARRAYS));
  // $app->response->headers->set('Content-Type', 'application/json');

  $getdata = $app->db->table('Trx_Record')->where("req_billingcode", $billingcode)->first();

  if ($getdata != NULL) {
    $data = $getdata["req_response"];
    $dataConvert = (array) json_decode($data);
    // echo $dataConvert["invoiceNo"];
    // die();
  } else {
    echo "Billing Code not Found or expired";
    die();
  }

  $payment = array(
  	"appsId" => "009",
  	"invoiceNo" => $dataConvert["invoiceNo"],
  	"routeId" => "001",
  	"TrxId" => "ANY-JK",
  	"UserId" => "0",
  	"Password" => "0",
  	"KodeBillingSimponi" => $billingcode,
  	"KodeKL" => "022",
  	"KodeEselon1" => "05",
  	"KodeSatker" => "465632",
  );

  $res = $client->BillingStatus($payment);
  $res = json_encode($res);
  $data = json_decode($res, true);
  $params = array('BillingCode' => $billingcode, 'StatusMessage' => $data["ResponseMessage"], "ExpiredDate" => $getdata["req_expired"]);
  $app->render("billingstatus.html", $params);
});
$app->get('/convert-rate', function() use ($app, $mssql) {

  function convertrate($k2_code, $app) {
    $access = $app->db->table("tx_kode_tarif_k2")->where("k2_code", $k2_code)->first();
    return $access["fare"];
  }

  $stmt = $mssql->prepare("SELECT wl_code FROM ms_tarif_payment");
  $stmt->execute();
  $row = $stmt->fetchAll();

  foreach ($row as $i) {
    // echo $i["wl_code"];
    $value = convertrate($i["wl_code"], $app);

    try {
      $stmt = $mssql->prepare("UPDATE ms_tarif_payment SET tarif=? WHERE wl_code=?");
      $id = $i["wl_code"];
      $stmt->execute(array($value,$id));
    } catch (PDOException $e) {
      echo $e->getMessage();
    }


  }

  // while ($row = $stmt->fetch()) {
  //   $value = convertrate($row["wl_code"], $app);
  //
  //   $stmt = $mssql->prepare("UPDATE ms_tarif_payment SET tarif=? WHERE wl_code=?");
  //   $id = $row["wl_code"];
  //   $stmt->execute(array($value,$id));
  //   sleep(10);
  //   // $value = convertrate($row["wl_code"], $app);;
  //   // echo $value." ".$id."\n";
  // }
});

// CRON
$app->get('/payment-test', function() use($app, $mssql) {
  // function rate_code_conversion($k2_code, $app, $output) {
  //   $access = $app->db->table("tx_kode_tarif_k2")->where("k2_code", $k2_code)->first();
  //   echo $access[$output];
  // }
  //
  // rate_code_conversion('WL01', $app, "unit");
  try {
    $stmt = $mssql->prepare("UPDATE tx_confirm_pembayaran SET flag=?, billing_code=? WHERE id=?");
    $flag = 1;
    $kodeBilling = "820151102273454";
    $idtx = "05DD581F-8513-402D-B4AB-26F8E8C1D329";
    $run = $stmt->execute(array($flag,$kodeBilling,$idtx));
    var_dump($run);
  } catch (PDOException $e) {
    echo $e->getMessage;
  }


});
$app->get('/payment-input', function() use($app,$mssql) {
  // Insert Log bahwa cron berjalan
  $log = $app->db->table("Log")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);
  // Inisiasi SOAP CLIENT
  $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");

  // Mengambil data yang akan dikirim ke SOA HubPayment pada database MS SQL dengan parameter flag = 0
  $stmt = $mssql->prepare("SELECT * FROM tx_confirm_pembayaran WHERE flag = 0 ORDER BY kode_tarif ASC");
  $stmt->execute();
  $log = $app->db->table("Log")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE MSSQL QUERY", "create_at" => time()
  ]);

  function rate_code_conversion($k2_code, $app, $output) {
    $access = $app->db->table("tx_kode_tarif_k2")->where("k2_code", $k2_code)->first();
    return $access[$output];
  }

  function convertNumber($number) {
    $c = explode('.', $number);
    return $c[0];
  }

  // Mengolah data yang diterima dari hasil query diatas
  while ($row = $stmt->fetch()) {
    // Enkapsulasi data menjadi Array yang nantinya dikirimkan ke SOA HubPayment
    $payment = array(
    	"appsId" => "009",
    	"invoiceNo" => "021/DKUPPU-PPO/".date("m")."/".date("Y"),
    	"routeId" => "001",
    	"data" => array(
          "PaymentHeader" => array(
            "TrxId" => "ANY-JK",
            "UserId" => "0",
          	"Password" => "0",
            "ExpiredDate" => date("Y-m-d H:i:s", time()+86400),
          	"DateSent" => date("Y-m-d H:i:s", time()),
          	"KodeKL" => "022",
          	"KodeEselon1" => "05",
          	"KodeSatker" => "465632",
            "JenisPNBP" => "F",
            "KodeMataUang" => "1",
            "TotalNominalBilling" => convertNumber($row["grand_total"]),
            "NamaWajibBayar" => $row["full_name"],
          ),
          // PaymentDetails - Data for all transaction
          "PaymentDetails" => array(
            // PaymentDetail - Place all data how many user transcation ( Bulk Input )
            "PaymentDetail" => array(
              "NamaWajibBayar" => $row["full_name"],
              "KodeTarifSimponi" => "00".rate_code_conversion($row["kode_tarif"], $app, 'fare_code'),
              "KodePPSimponi" => "2015011",
              "KodeAkun" => "423216",
              "TarifPNBP" => rate_code_conversion($row["kode_tarif"], $app, 'fare'),
              "Volume" => $row["volume"],
              "Satuan" => rate_code_conversion($row["kode_tarif"], $app, 'unit'),
              "TotalTarifPerRecord" => rate_code_conversion($row["kode_tarif"], $app, 'fare') * $row["volume"],
            ),
          ),
        ),

    );
    // Merubah array yang diatas menjadi format JSON
    $paymentJson = json_encode($payment);
    // Mengirim ke fungsi SOA HubPayment
    $res = $client->PaymentRequest($payment);
    // Data yang diterima dari fungsi SOA HubPayment diconvert menjadi JSON
    // $res = json_encode($res);
    // Data yang tadi diterima sudah menjadi JSON lalu dirubah menjadi Array

    $data = json_decode(json_encode($res), true);
    $dataJson = json_encode($data);
    $log = $app->db->table("Log")->insertGetId([
        "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE SOAP FUNCTION AND DECODE", "create_at" => time()
    ]);

    // $app->response->headers->set('Content-Type', 'application/json');
    // print_r($res);
    // Jika response code sama dengan 00 ( sukses )

    if($data["response"]["code"] == "00") {
      // Simpan data pada database MySQL local API Payment sebagai arsip
      $log = $app->db->table('Trx_Record')->insertGetId([
        'req_time' => time(), 'req_entity' => $paymentJson, 'req_response' => $dataJson, 'req_response_code' => $data["response"]["code"], 'req_billingcode' => $data["response"]["simponiData"]["KodeBillingSimponi"], 'req_expired' => $data["response"]["simponiData"]["ExpiredDate"]
      ]);
      // Update data yang berada pada database MS SQL, memasukkan Billing Code dan merubah flag = 1

      try {
        // $stmt = $mssql->prepare("UPDATE tx_confirm_pembayaran SET flag = :flag, billing_code = :billingcode WHERE id = :id_tx");
        // $flag = 1;
        // $stmt->bindParam(':flag', $flag, PDO::PARAM_STR);
        // $kodeBilling = "N'".$data["response"]["simponiData"]["KodeBillingSimponi"]."'";
        // $stmt->bindParam(':billingcode', $kodeBilling, PDO::PARAM_STR);
        // $idtx = "N'".$row["id"]."'";
        // $stmt->bindParam(':id_tx', $idtx, PDO::PARAM_STR);
        // $stmt->execute();

        $stmt = $mssql->prepare("UPDATE tx_confirm_pembayaran SET flag=?, billing_code=? WHERE id=?");
        $flag = 1;
        $kodeBilling = $data["response"]["simponiData"]["KodeBillingSimponi"];
        $idtx = $row["id"];
        $stmt->execute(array($flag,$kodeBilling,$idtx));
      } catch (PDOException $e) {
        echo $e->getMessage();
      }


      echo "Success Input Payment with ID ". $idtx .", Billing Code ". $kodeBilling;
    } else {
      // Jika response code bukan sama dengan 00 maka akan disimpan pada database MySQL untuk tindak lanjut
      $log = $app->db->table('Trx_Record')->insertGetId([
        'req_time' => time(), 'req_entity' => $paymentJson, 'req_response_code' => $data["response"]["code"]
      ]);
      var_dump($payment);
      die();
    }
  }
});

$app->get('/payment-input-test', function() use($app,$mssql) {
  // Insert Log bahwa cron berjalan
  // $log = $app->db->table("Log")->insertGetId([
  //     "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  // ]);
  // Inisiasi SOAP CLIENT
  // $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");

  // Mengambil data yang akan dikirim ke SOA HubPayment pada database MS SQL dengan parameter flag = 0
  $stmt = $mssql->prepare("SELECT * FROM tx_confirm_pembayaran WHERE flag = 0");
  $stmt->execute();
  $data = $stmt->fetch(PDO::FETCH_OBJ);

  var_dump($data);

});
$app->get('/payment-status', function() use($app,$mssql) {
  $log = $app->db->table("Log")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_STATUS", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);


});

// V2
$app->post('/paymentrequest', function() use($app,$mssql) {
  header("access-control-allow-origin: *");
  $trxid = $app->request->post("id");

  // Insert Log bahwa cron berjalan
  $log = $app->db->table("Log")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "REQUEST RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);

  // Inisiasi SOAP CLIENT
  $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");

  // Mengambil data yang akan dikirim ke SOA HubPayment
  $stmt = $mssql->prepare("SELECT * FROM t_trx_license_invoice WHERE id_trx_lic_invoice = ?");
  $stmt->execute(array($trxid));
  $data = $stmt->fetchAll();
  // Mengambil detail dari data invoice
  $stmtdtl = $mssql->prepare("SELECT * FROM t_trx_license_invoice_dtl WHERE id_trx_lic_invoice = ?");
  $stmtdtl->execute(array($trxid));
  $datadtl = $stmtdtl->fetchAll();
  // Mengambil ID User
  $userdtl = $mssql->prepare("SELECT * FROM t_mst_user_login WHERE username = ?");
  $userdtl->execute(array($data[0]["created_by"]));
  $udtl = $userdtl->fetchAll();
  // Mengambil Email User
  $uedtl = $mssql->prepare("SELECT * FROM t_mst_user WHERE id_user = ?");
  $uedtl->execute(array($udtl[0]["id_user"]));
  $umdtl = $uedtl->fetchAll();

  $log = $app->db->table("Log")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE MSSQL QUERY", "create_at" => time()
  ]);
  $expired_date = date("Y-m-d H:i:s", time()+86400);
  $sent_date = date("Y-m-d H:i:s", time()+86400);
  $payment = array(
  	"appsId" => "009",
  	"invoiceNo" => $data[0]["invoice_no"],
  	"routeId" => "001",
  	"data" => array(
        "PaymentHeader" => array(
          "TrxId" => "ANY-JK",
          "UserId" => "0",
        	"Password" => "0",
          "ExpiredDate" => $expired_date,
        	"DateSent" => $sent_date,
        	"KodeKL" => "022",
        	"KodeEselon1" => "05",
        	"KodeSatker" => "465632",
          "JenisPNBP" => "F",
          "KodeMataUang" => "1",
          "TotalNominalBilling" => $data[0]["total_price"],
          "NamaWajibBayar" => $data[0]["pic_name"],
        ),
        // PaymentDetails - Data for all transaction
        "PaymentDetails" => array(),
      ),

  );
  $num = 0;
  foreach ($datadtl as $row) {
    $payment["data"]["PaymentDetails"]["PaymentDetail"][] = array(
        "NamaWajibBayar" => $row["nm_applicant"],
        "KodeTarifSimponi" => "00".$row["simponi_code"],
        "KodePPSimponi" => "2015011",
        "KodeAkun" => "423216",
        "TarifPNBP" => $row["pnbp_price"],
        "Volume" => $row["volume"],
        "Satuan" => $row["lic_unit"],
        "TotalTarifPerRecord" => $row["pnbp_price"] * $row["volume"],
      );
    $num++;
  }

  // Merubah array yang diatas menjadi format JSON
  $paymentJson = json_encode($payment);
  // Mengirim ke fungsi SOA HubPayment
  $res = $client->PaymentRequest($payment);
  // Data yang diterima dari fungsi SOA HubPayment diconvert menjadi JSON
  // $res = json_encode($res);
  // Data yang tadi diterima sudah menjadi JSON lalu dirubah menjadi Array

  $data = json_decode(json_encode($res), true);
  $dataJson = json_encode($data);
  $log = $app->db->table("Log")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE SOAP FUNCTION AND DECODE", "create_at" => time()
  ]);

  $app->response->headers->set('Content-Type', 'application/json');
  // print_r($res);

  // Jika response code sama dengan 00 ( sukses )
  if($data["response"]["code"] == "00") {
    // Simpan data pada database MySQL local API Payment sebagai arsip
    $log = $app->db->table('Trx_Record')->insertGetId([
      'req_time' => time(), 'req_entity' => $paymentJson, 'req_response' => $dataJson, 'req_response_code' => $data["response"]["code"], 'req_billingcode' => $data["response"]["simponiData"]["KodeBillingSimponi"], 'req_expired' => $data["response"]["simponiData"]["ExpiredDate"]
    ]);
    // Update data yang berada pada database MS SQL, memasukkan Billing Code
    try {
      $stmt = $mssql->prepare("UPDATE t_trx_license_invoice SET billing_code=?, expired_date=? WHERE id_trx_lic_invoice=?");
      $kodeBilling = $data["response"]["simponiData"]["KodeBillingSimponi"];
      $stmt->execute(array($kodeBilling,$expired_date,$trxid));
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
    $return = array('result' => true, 'id_invoice' => $trxid, 'billing_code' => $kodeBilling);
    echo json_encode($return);

    $url = "http://10.0.48.31/mail/".$umdtl[0]["fullname"]."/". $umdtl[0]["email_address"] ."/". $kodeBilling;
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    // curl_setopt($ch,CURLOPT_HEADER, false);
    $output = curl_exec($ch);
    curl_close($ch);

  } else {
    // Jika response code bukan sama dengan 00 maka akan disimpan pada database MySQL untuk tindak lanjut
    $log = $app->db->table('Trx_Record')->insertGetId([
      'req_time' => time(), 'req_entity' => $paymentJson, 'req_response_code' => $data["response"]["code"]
    ]);
    var_dump($payment);
    die();
  }

});

$app->map('/checker', function() use($app, $mssql) {
  $req = $app->request->getMethod();
  if ($req == 'GET') {
    return $app->render('idchecker.html');
  } elseif ($req == 'POST') {
    $nik = $app->request->post('applicantid');

    $stmt = $mssql->prepare("SELECT * FROM t_mst_applicant WHERE applicant_nik = ?");
    $stmt->execute(array($nik));
    // $data = $stmt->fetch(PDO::FETCH_OBJ);
    $data = $stmt->fetchAll();
    // $data = (stdClass) $data;
    // echo $data->fullname;
    // echo json_encode($data);
    // var_dump($data);
    // die();
    // header('Content-Type: text/plain; charset=ISO-8859-1');
    $params = array('applicant_name' => $data[0]['fullname'], );
    $value = $data[0]['fullname'];
    $encode = mb_detect_encoding($value, mb_detect_order(), true);
    echo $encode;

    // echo iconv("Latin1_General_CI_AS", "ISO-8859-1", $data[0]['fullname']);
    // return $app->render('idchecker.html', $params);
  }
})->via('GET', 'POST');
// Mail Handler
$app->get('/mail/:orgname/:mailaddr/:billingcode', function($orgname, $mailaddr, $billingcode) use($app, $mail) {

  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtpgw.dephub.go.id';                  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = 'admin.dkuppu@dephub.go.id';                 // SMTP username
  $mail->Password = 'dephub';

  $mail->Port = 25;

  $mail->setFrom('notif@dephub.go.id', 'PPO DEPHUB');
  $mail->addAddress($mailaddr);

  $mail->addCC('jismil.lathif@gmail.com');
  $mail->addCC('syapril.yunus0104@gmail.com');
  $mail->addCC('alvaryno@gmail.com');
  $mail->addCC('rudianto@gmail.com');
  $mail->addCC('great.wahyu@gmail.com');
  $mail->addCC('sofian.hadie@gmail.com');
  $mail->addCC('andriaripratama@gmail.com');
  $mail->addCC('bowow@gmail.com');

  $mail->isHTML(true);

  $mail->Subject = 'Permintaan Anda Pada Perizinan Online [TESTING FROM PAYMENT GATEWAY]';
  $mail->Body    = 'Yth. '.$orgname.',<br><br>Kami menerima permintaan anda atas pengajuan penerbitan lisensi baru, sebelum dapat diproses harap melakukan pembayaran dengan Kode Billing <strong>'.$billingcode.'</strong> pada Bank BRI.<br><br>Hormat Kami,<br><br>PPO Dephub';
  // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

  if(!$mail->send()) {
      echo 'Message could not be sent.';
      echo 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
      return true;
  }

});

$app->run();

 ?>
