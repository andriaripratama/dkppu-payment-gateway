<?php

require "app/init.php";

$app->get('/', function() use($app) {
  echo "HELLO";
});

$app->map('/login', function() use($app) {
  $_req = $app->request->getMethod();

  if ($_req == "GET") {
    $params = array('title' => 'DEPHUB CAT', 'sub_domain' => 'cat', );
    $app->render("login.html", $params);
  } elseif ($_req == "POST") {
    $user = $app->request->post('cat_username');
    $pass = $app->request->post('cat_password');
    $user_data = $app->db->table('User')->where('username', $user)->where('is_active', 1)->first();
    if($user_data != null) {
      // $pass = hash('SHA256', $pass);
      var_dump($user_data);
      if (hash('SHA256',$pass) == $user_data["password"]) {
        $log = $app->db->table('User')->where('username', $user)->update(array('last_login' => time()));
        $_SESSION["dephub_cat_logged_user"] = $user;
        $_SESSION["dephub_cat_logged_id"] = $user_data["id"];
        $_SESSION["dephub_cat_logged_type"] = $user_data["type"];
        $_SESSION["dephub_cat_session_exp"] = time() + 7200;

        switch ($user_data["type"]) {
          case '101':
            $app->redirect('admin');
            break;
          case '102':

            break;
          case '103':
            $app->redirect('entrant');
            break;
          default:
            # code...
            break;
        }

      }
    } else {
      $app->flash('flash_message_error', 'Username atau Password salah! Coba lagi.');
      $app->redirect('/cat/login');
    }

  }
})->via('GET', 'POST');

$app->get('/entrant', function() use($app) {
  // require "app/database.php";

  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "103" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {
        $_getUserData = $app->db->table('ParticipantData')->where('participant_id', $_SESSION["dephub_cat_logged_user"])->first();
        $_checkQuestionTemp = $app->db->table('ExamQuestionTemp')->where('user_id', $_SESSION["dephub_cat_logged_user"])->get();
        if($_checkQuestionTemp == NULL) {
          $_getQuestionData = $app->db->table('ExamQuestionBank')->select('course_id','question','option_one','option_two','option_three','option_four','option_five')->where('course_id', $_getUserData["course_id"])->orderByRaw("RAND()")->get();
          foreach ($_getQuestionData as $r) {
            $r["user_id"] = $_SESSION["dephub_cat_logged_user"];
            $r["created_at"] = time();
            $_insertTempQuestion = $app->db->table('ExamQuestionTemp')->insert($r);
          }
        }
        $_countQuestion = $app->db->table('ExamQuestionTemp')->select("id")->where('user_id', $_SESSION["dephub_cat_logged_user"])->get();

        $params = array('sub_domain' => 'cat', 'count' => $_countQuestion, );
        $app->render('participant_question.html', $params);

      }
    } else {
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
      session_unset();
      session_destroy();
    }
  }
});

$app->get('/admin', function() use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {

        $params = array('title' => 'ADMIN | DEPHUB CAT', 'sub_domain' => 'cat', 'session_user_logged_name' => $_SESSION["dephub_cat_logged_user"],);
        $app->render('admin_index.html', $params);

      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
});

$app->get('/admin/question', function() use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {
        $data_course = $app->db->table('Course')->get();
        $params = array('title' => 'ADMIN | QUESTION | DEPHUB CAT',
                        'sub_domain' => 'cat',
                        'session_user_logged_name' => $_SESSION["dephub_cat_logged_user"],
                        'data_course' => $data_course, );
        $app->render('admin_question.html', $params);

      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
});

$app->get('/admin/question/:id', function($id) use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {
        $data_question = $app->db->table('ExamQuestionBank')->where('course_id',$id)->get();
        $params = array('title' => 'ADMIN | QUESTION | DEPHUB CAT',
                        'sub_domain' => 'cat',
                        'session_user_logged_name' => $_SESSION["dephub_cat_logged_user"],
                        'data_question' => $data_question,
                        'course_id' => $id,);
        $app->render('admin_question_list.html', $params);

      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
});

$app->map('/admin/question/add/:id', function($id) use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {
        $_req = $app->request->getMethod();
        if ($_req == "GET") {
          $params = array('title' => 'ADMIN | QUESTION | DEPHUB CAT',
                          'sub_domain' => 'cat',
                          'session_user_logged_name' => $_SESSION["dephub_cat_logged_user"],
                          'course_id' => $id, );

          $app->render('admin_question_add.html', $params);
        } elseif ($_req == "POST") {
          $_POST["course_id"] = $id;
          $_POST["created_by"] = $_SESSION["dephub_cat_logged_user"];
          $_POST["created_at"] = time();

          $insert = $app->db->table('ExamQuestionBank')->insert($_POST);
          if($insert) {
            echo "Success";
          } else {
            echo "Failed";
          }
        }


      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
})->via('GET', 'POST');

$app->get('/admin/course', function() use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {

        $data_course = $app->db->table('Course')->get();
        $params = array('title' => 'ADMIN | DEPHUB CAT',
                        'sub_domain' => 'cat',
                        'session_user_logged_name' => $_SESSION["dephub_cat_logged_user"],
                        'data_course' => $data_course, );
        $app->render('admin_course.html', $params);

      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
});

$app->map('/admin/course/add', function() use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {

        $_req = $app->request->getMethod();

        if($_req == "GET") {
          $params = array('title' => 'ADMIN | COURSE | DEPHUB CAT',
                          'sub_domain' => 'cat',
                          );
          $app->render('admin_course_add.html', $params);
        } elseif ($_req == "POST") {
          $_POST["created_by"] = $_SESSION["dephub_cat_logged_user"];
          $_POST["created_at"] = time();
          $insert = $app->db->table('Course')->insert($_POST);

          if($insert) {
            $app->flash('flash_message', 'Suskes menyimpan.');
            $app->redirect("/cat/admin/course");
          } else {
            $app->flash('flash_message_error', 'Gagal menyimpan.');
            $app->redirect("/cat/admin/course");
          }
        }

      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
})->via('GET', 'POST');

$app->get('/admin/group', function() use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {

        $params = array('title' => 'ADMIN | DEPHUB CAT', 'sub_domain' => 'cat', );
        $app->render('admin_group.html', $params);

      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
});

$app->get('/admin/participant', function() use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {
        $data_participant = $app->db->table("ParticipantData")->get();
        $params = array('title' => 'ADMIN | DEPHUB CAT',
                        'sub_domain' => 'cat',
                        'session_user_logged_name' => $_SESSION["dephub_cat_logged_user"],
                        'data_participant' => $data_participant,
                        );
        $app->render('admin_participant.html', $params);

      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
});

$app->map('/admin/participant/add', function() use($app) {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    if ( $_SESSION["dephub_cat_logged_type"] == "101" ) {
      if( $_SESSION["dephub_cat_session_exp"] < time() ) {
        $app->flash('flash_message_error', 'Sesi anda berakhir, silahkan mengulangi masuk.');
        $app->redirect('/cat/login');
      } else {
        $_req = $app->request->getMethod();

        $data_course = $app->db->table('Course')->get();

        if ($_req == "GET") {
          $params = array('title' => 'ADMIN | DEPHUB CAT',
                          'sub_domain' => 'cat',
                          'session_user_logged_name' => $_SESSION["dephub_cat_logged_user"],
                          'data_course' => $data_course,
                          );
          $app->render('admin_participant_add.html', $params);
        } elseif ($_req == "POST") {
          $_POST["type"] = "103";
          $_POST["created_by"] = $_SESSION["dephub_cat_logged_user"];
          $_POST["created_at"] = time();

          $_participantData = array("course_id" => $_POST["course_id"], "participant_name" => $_POST["participant_name"], "created_by" => $_SESSION["dephub_cat_logged_user"], "created_at" => time(), );
          $insert = $app->db->table('ParticipantData')->insertGetId($_participantData);
          $_participantID = "11".$insert."".date("Y");
          $update = $app->db->table('ParticipantData')->where('id', $insert)->update(array("participant_id" => $_participantID,));

          // Stored to user table
          $_userData = array('username' => $_participantID, 'password' => hash('sha256', '1234'), 'type' => $_POST["type"], 'is_active' => 1, 'created_at' => time(), );
          $insert = $app->db->table('User')->insert($_userData);

          if($insert) {
            $app->flash('flash_message', 'Suskes menyimpan.');
            $app->redirect("/cat/admin/participant");
          } else {
            $app->flash('flash_message_error', 'Gagal menyimpan.');
            $app->redirect("/cat/admin/participant");
          }
        }

      }
    } else {
      session_unset();
      session_destroy();
      $app->flash('flash_message_error', 'Anda tidak diizinkan, user anda keluar secara otomatis.');
      $app->redirect('/cat/login');
    }
  }
})->via("GET", "POST");

$app->get('/logout', function() use($app)  {
  if(!isset($_SESSION["dephub_cat_logged_id"])) {
    $app->flash('flash_message_error', 'Silahkan masuk telebih dahulu.');
    $app->redirect('/cat/login');
  } else {
    session_unset();
    session_destroy();
    $app->redirect('/cat/login');
  }
});

// CAT API!!!
$app->post('/api/question/get', function() use($app) {
  $questionID = $app->request->post('id');

  $getQuestion = $app->db->table('ExamQuestionTemp')->where('id', $questionID)->first();
  echo json_encode($getQuestion);
});

$app->run();

 ?>
