#!/bin/bash

PIDS=`ps aux | grep cron-payment.php | grep -v grep`
if [ -z "$PIDS" ]; then
    php /home/app/cron-payment.php &
else
    echo "cron-payment.php already running."
fi
