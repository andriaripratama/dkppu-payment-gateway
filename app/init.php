<?php
error_reporting(-1);
ini_set('display_errors', 'On');
date_default_timezone_set("Asia/Jakarta");
define("DEV", "TRUE");
use Illuminate\Database\Capsule\Manager as Capsule;

session_start();

require_once 'vendor/autoload.php';
$mail = new PHPMailer;
require "app/database.php";
require "app/database-mssql.php";

$app = new Slim\Slim(['templates.path' => 'app/views']);
$app->view = new \Slim\Views\Twig();
$app->view->setTemplatesDirectory("app/views");

$view = $app->view();
$view->parserOptions = ['debug' => true];
$view->parserExtensions = [new \Slim\Views\TwigExtension()];

$app->db = function() {
	return new Capsule;
}
?>
