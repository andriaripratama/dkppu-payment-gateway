<?php
// require_once 'vendor/autoload.php';
// require 'app/init-cron.php';
//
// $timeSent = time();
// $timeExp = time()+172800;
//
// // Insert Log bahwa cron berjalan
// $log = $app->db->table("Log")->insertGetId([
//     "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
// ]);
// // Inisiasi SOAP CLIENT
// $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");
//
// // Mengambil data yang akan dikirim ke SOA HubPayment pada database MS SQL dengan parameter flag = 0
// $stmt = $mssql->prepare("SELECT * FROM tx_confirm_pembayaran WHERE flag = 0 ORDER BY kode_tarif ASC");
// $stmt->execute();
// $log = $app->db->table("Log")->insertGetId([
//     "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE MSSQL QUERY", "create_at" => time()
// ]);
//
// function rate_code_conversion($k2_code, $app, $output) {
//   $access = $app->db->table("tx_kode_tarif_k2")->where("k2_code", $k2_code)->first();
//   return $access[$output];
// }
//
// function convertNumber($number) {
//   $c = explode('.', $number);
//   return $c[0];
// }
//
// // Mengolah data yang diterima dari hasil query diatas
// while ($row = $stmt->fetch()) {
//   // $log = $app->db->table("Log_MSSQL")->insertGetId([
//   //     "type" => "INFO_LOG_PAYMENT_MSSQL", "message" => $row["nomor_invoice"], "create_at" => time()
//   // ]);
//   // Enkapsulasi data menjadi Array yang nantinya dikirimkan ke SOA HubPayment
//   $payment = array(
//     "appsId" => "009",
//     "invoiceNo" => $row["nomor_invoice"],
//     "routeId" => "001",
//     "data" => array(
//         "PaymentHeader" => array(
//           "TrxId" => "ANY-JK",
//           "UserId" => "0",
//           "Password" => "0",
//           "ExpiredDate" => date("Y-m-d H:i:s", $timeExp),
//           "DateSent" => date("Y-m-d H:i:s", $timeSent),
//           "KodeKL" => "022",
//           "KodeEselon1" => "05",
//           "KodeSatker" => "465632",
//           "JenisPNBP" => "F",
//           "KodeMataUang" => "1",
//           "TotalNominalBilling" => convertNumber($row["grand_total"]),
//           "NamaWajibBayar" => $row["full_name"],
//         ),
//         // PaymentDetails - Data for all transaction
//         "PaymentDetails" => array(
//           // PaymentDetail - Place all data how many user transcation ( Bulk Input )
//           "PaymentDetail" => array(
//             "NamaWajibBayar" => $row["full_name"],
//             "KodeTarifSimponi" => "00".rate_code_conversion($row["kode_tarif"], $app, 'fare_code'),
//             "KodePPSimponi" => "2015011",
//             "KodeAkun" => "423216",
//             "TarifPNBP" => rate_code_conversion($row["kode_tarif"], $app, 'fare'),
//             "Volume" => $row["volume"],
//             "Satuan" => rate_code_conversion($row["kode_tarif"], $app, 'unit'),
//             "TotalTarifPerRecord" => rate_code_conversion($row["kode_tarif"], $app, 'fare') * $row["volume"],
//           ),
//         ),
//       ),
//
//   );
//   // Merubah array yang diatas menjadi format JSON
//   $paymentJson = json_encode($payment);
//   // Mengirim ke fungsi SOA HubPayment
//   $res = $client->PaymentRequest($payment);
//   // Data yang diterima dari fungsi SOA HubPayment diconvert menjadi JSON
//   // $res = json_encode($res);
//   // Data yang tadi diterima sudah menjadi JSON lalu dirubah menjadi Array
//
//   $data = json_decode(json_encode($res), true);
//   $dataJson = json_encode($data);
//   $log = $app->db->table("Log")->insertGetId([
//       "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE SOAP FUNCTION AND DECODE", "create_at" => time()
//   ]);
//
//   // $app->response->headers->set('Content-Type', 'application/json');
//   // print_r($res);
//   // Jika response code sama dengan 00 ( sukses )
//
//   if($data["response"]["code"] == "00") {
//     // Simpan data pada database MySQL local API Payment sebagai arsip
//     $log = $app->db->table('Trx_Record')->insertGetId([
//       'req_time' => time(), 'req_entity' => $paymentJson, 'req_response' => $dataJson, 'req_response_code' => $data["response"]["code"], 'req_billingcode' => $data["response"]["simponiData"]["KodeBillingSimponi"], 'req_expired' => $data["response"]["simponiData"]["ExpiredDate"]
//     ]);
//     // Update data yang berada pada database MS SQL, memasukkan Billing Code dan merubah flag = 1
//
//     try {
//
//       $stmt = $mssql->prepare("UPDATE tx_confirm_pembayaran SET flag=?, billing_code=? WHERE id=?");
//       $flag = 1;
//       $kodeBilling = $data["response"]["simponiData"]["KodeBillingSimponi"];
//       $idtx = $row["id"];
//       $stmt->execute(array($flag,$kodeBilling,$idtx));
//
//     } catch (PDOException $e) {
//       echo $e->getMessage();
//     }
//
//     $mail->isSMTP();
//     $mail->Host = 'smtpgw.dephub.go.id';
//     $mail->SMTPAuth = true;
//     $mail->Username = 'admin.dkuppu@dephub.go.id';
//     $mail->Password = 'dephub';
//
//     $mail->Port = 25;
//
//     $mail->setFrom('notif@dephub.go.id', 'PPO-DEPHUB');
//     $mail->addAddress($row["email_address"]);
//
//     $mail->addCC('andriaripratama@gmail.com');
//     // $mail->addCC('jismil.lathif@gmail.com');
//     $mail->addCC('syapril.yunus0104@gmail.com');
//     $mail->addCC('budy.kusumah@gmail.com');
//     // $mail->addCC('alvaryno@gmail.com');
//     $mail->addCC('great.wahyu@gmail.com');
//     $mail->addCC('sofian.hadie@gmail.com');
//     // $mail->addCC('airputihdingin@gmail.com');
//
//     $mail->isHTML(true);
//
//     $mail->Subject = 'Permintaan Anda Pada Perizinan Online [TESTING FROM PAYMENT GATEWAY]';
//     $mail->Body    = 'Bpk/Ibu '.$row["full_name"].' ,<br><br>Kami menerima permintaan anda atas pengajuan penerbitan lisensi baru, sebelum dapat diproses harap melakukan pembayaran dengan Kode Billing <strong>'.$data["response"]["simponiData"]["KodeBillingSimponi"].'</strong> pada Bank BRI.<br><br>Hormat Kami,<br><br>PPO Dephub';
//     // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
//
//     if(!$mail->send()) {
//       echo 'Message could not be sent.';
//       echo 'Mailer Error: ' . $mail->ErrorInfo;
//       $log = $app->db->table("Log")->insertGetId([
//           "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", FAILED MAIL TO ".$row["email_address"].", REASON : ".$mail->ErrorInfo, "create_at" => time()
//       ]);
//     } else {
//       echo 'Message has been sent';
//       $log = $app->db->table("Log")->insertGetId([
//           "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS MAIL TO ".$row["email_address"], "create_at" => time()
//       ]);
//     }
//   } else {
//     // Jika response code bukan sama dengan 00 maka akan disimpan pada database MySQL untuk tindak lanjut
//     $log = $app->db->table('Trx_Record')->insertGetId([
//       'req_time' => time(), 'req_entity' => $paymentJson, 'req_response_code' => $data["response"]["code"], 'req_message' => $data["response"]["message"]
//     ]);
//     var_dump($data);
//   }
// }

?>
