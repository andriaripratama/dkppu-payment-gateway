<?php

// KEMENKEU SOA HubPayment Bridging
// KEMENKEU SIMPONI BRI
// DEPHUB-JTN-TELKOM
// Andri Ari Pratama
// me@aap.my.id
// 2015

require 'app/init.php';

$app->get('/', function() use ($app,$mssql) {
  // $app->response->headers->set('Content-Type', 'application/json');
  // phpinfo();
  // echo $hostname;
  echo $_SERVER["REMOTE_ADDR"];
});

$app->get('/testing', function() use ($app, $mssql) {
  echo DEV;
});

// V2
$app->get('/invoice/:trx_number', function($trx_number) use ($app) {
  $data = $app->db->table('Trx_Record')->where("id_mssql", $trx_number)->orderBy("id","desc")->take(1)->get();
  // $data_response = (array) json_decode($data[0]["req_response"]);
  // $data_billing_response = (array) json_decode($data[0]["req_billing_response"]);
  // var_dump(json_encode($data_response));
  $ds["data"] = array("id" => $data[0]["id"],
                      "id_mssql" => $data[0]["id_mssql"],
                      "reqTime" => $data[0]["req_time"],
                      "reqEntity" => json_decode($data[0]["req_entity"], true),
                      "reqResponse" => json_decode($data[0]["req_response"], true),
                      "reqBillingResponse" => json_decode($data[0]["req_billing_response"], true),);
  // $params = array("dr" => $data_response, "dbr" => $data_billing_response,);
  // var_dump(json_encode($ds));
  $app->render("invoice.html", $ds);
});

$app->get('/blsr', function() use($app, $mssql) {


  // $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl", array('cache_wsdl' => WSDL_CACHE_NONE, 'features'=>SOAP_SINGLE_ELEMENT_ARRAYS));
  $client = new SoapClient("http://10.47.210.17:7800/SimponiBRI_Service?wsdl");

  $getdata = $app->db->table('Trx_Record')->where("req_billingcode", "820160224318268")->get();
  // $getdata = $app->db->table('Trx_Record')->where("req_paid", "0")->whereNotNull("req_response")->orderBy("id", "desc")->get();
  // var_dump($getdata);
  // die();
  if ($getdata != NULL) {
    foreach ($getdata as $r) {
      $exp = strtotime($r["req_expired"]);

      if ($exp < time()) {
        $data = $r["req_response"];
        $dataConvert = (array) json_decode($data);

        $payment = array(
        	"appsId" => "009",
        	"invoiceNo" => $dataConvert["invoiceNo"],
        	"routeId" => "001",
        	"TrxId" => "ANY-JK",
        	"UserId" => "0",
        	"Password" => "0",
        	"KodeBillingSimponi" => $r["req_billingcode"],
        	"KodeKL" => "022",
        	"KodeEselon1" => "05",
        	"KodeSatker" => "465632",
        );
        $res = $client->BillingStatus($payment);
        $res = json_encode($res);
        $data = json_decode($res, true);
        var_dump($data);
        die();
        if($data["ResponseCode"] === "00") {
          $update = $app->db->table("Trx_Record")->where("req_billingcode", $r["req_billingcode"])->update(array('req_paid' => 1, 'req_billing_response' => $res, 'req_ntb' => $data["ResponseData"]["NTB"], 'req_ntpn' =>$data["ResponseData"]["NTPN"]));

          // echo "Pay<br>";
          try {
            $stmt = $mssql->prepare("UPDATE t_trx_license_invoice SET is_paid=?, ntb=?, ntpn=?, paid_date=? WHERE billing_code=?");
            $is_paid = 1;
            $billing_code = $r["req_billingcode"];
            $ntb = $data["ResponseData"]["NTB"];
            $ntpn = $data["ResponseData"]["NTPN"];
            $trxdate = $data["ResponseData"]["TrxDate"];
            $stmt->execute(array($is_paid,$ntb,$ntpn,$trxdate,$billing_code));
          } catch (PDOException $e) {
            // echo $e->getMessage();
            $log = $app->db->table("Log")->insertGetId([
                "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "ERROR OF MSSQL : ".$e->getMessage(), "create_at" => time()
            ]);
          }
        } else {
          // echo "No Pay yet<br>";
        }
      } else {
        // Expired
      } // Time Checker
    } // Foreach
  } else {
    $log = $app->db->table("Log")->insertGetId([
        "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "NO NEW DATA", "create_at" => time()
    ]);
    die();
  }
});
$app->get('/billingstatusrequest', function() use ($app, $mssql) {
  $log = $app->db->table("Log")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "CRON BILLING CHECKER RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);
  // $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl", array('cache_wsdl' => WSDL_CACHE_NONE, 'features'=>SOAP_SINGLE_ELEMENT_ARRAYS));
  $client = new SoapClient("http://10.47.210.17:7800/SimponiBRI_Service?wsdl", array('cache_wsdl' => WSDL_CACHE_NONE, 'features'=>SOAP_SINGLE_ELEMENT_ARRAYS));

  $log = $app->db->table("Log")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "CRON BILLING CHECKER SOAP CONNECTED & RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);
  // $getdata = $app->db->table('Trx_Record')->where("req_billingcode", "820160108315455")->get();
  $getdata = $app->db->table('Trx_Record')->where("req_paid", "0")->whereNotNull("req_response")->orderBy("id", "desc")->get();
  // var_dump($getdata);
  // die();
  if ($getdata != NULL) {
    foreach ($getdata as $r) {
      $exp = strtotime($r["req_expired"]);

      if ($exp > time()) {
        $data = $r["req_response"];
        $dataConvert = (array) json_decode($data);

        $payment = array(
        	"appsId" => "009",
        	"invoiceNo" => $dataConvert["invoiceNo"],
        	"routeId" => "001",
        	"TrxId" => "ANY-JK",
        	"UserId" => "0",
        	"Password" => "0",
        	"KodeBillingSimponi" => $r["req_billingcode"],
        	"KodeKL" => "022",
        	"KodeEselon1" => "05",
        	"KodeSatker" => "465632",
        );
        $res = $client->BillingStatus($payment);
        $res = json_encode($res);
        $data = json_decode($res, true);

        if($data["ResponseCode"] === "00") {
          $update = $app->db->table("Trx_Record")->where("req_billingcode", $r["req_billingcode"])->update(array('req_paid' => 1, 'req_billing_response' => $res, 'req_ntb' => $data["ResponseData"]["NTB"], 'req_ntpn' =>$data["ResponseData"]["NTPN"]));

          // echo "Pay<br>";
          try {
            $stmt = $mssql->prepare("UPDATE t_trx_license_invoice SET is_paid=?, ntb=?, ntpn=?, paid_date=? WHERE billing_code=?");
            $is_paid = 1;
            $billing_code = $r["req_billingcode"];
            $ntb = $data["ResponseData"]["NTB"];
            $ntpn = $data["ResponseData"]["NTPN"];
            $trxdate = $data["ResponseData"]["TrxDate"];
            $stmt->execute(array($is_paid,$ntb,$ntpn,$trxdate,$billing_code));
          } catch (PDOException $e) {
            // echo $e->getMessage();
            $log = $app->db->table("Log")->insertGetId([
                "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "ERROR OF MSSQL : ".$e->getMessage(), "create_at" => time()
            ]);
          }
        } else {
          // echo "No Pay yet<br>";
        }
      } else {
        // Expired
      } // Time Checker
    } // Foreach
  } else {
    $log = $app->db->table("Log")->insertGetId([
        "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "NO NEW DATA", "create_at" => time()
    ]);
    die();
  }
});

$app->post('/paymentrequest', function() use($app,$mssql,$mail) {
  header("access-control-allow-origin: *");
  // $trxid = $app->request->post("id");
  // $_ip = $_SERVER["HTTP_ORIGIN"];
  // echo($_ip);
  // die();

  $log = $app->db->table("Log_V2")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "ID RECEIVED ".$app->request->getBody(), "create_at" => time()
  ]);

  $trxid = $_POST["id"];

  $log = $app->db->table("Log_V2")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "REQUEST RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);

  // Inisiasi SOAP CLIENT
  // $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");
  $client = new SoapClient("http://10.47.210.17:7800/SimponiBRI_Service?wsdl");

  $log = $app->db->table("Log_V2")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "SOAP CONNECTED AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);
  // Mengambil data yang akan dikirim ke SOA HubPayment
  $stmt = $mssql->prepare("SELECT * FROM t_trx_license_invoice WHERE id_trx_lic_invoice = ?");
  $stmt->execute(array($trxid));
  $data = $stmt->fetchAll();
  
  if ($data != NULL) {
    $log = $app->db->table("Log_V2")->insertGetId([
        "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "INVOICE RETURN TRUE : ".$trxid, "create_at" => time()
    ]);
  } else {
    $log = $app->db->table("Log_V2")->insertGetId([
        "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "INVOICE RETURN NULL : ".$trxid, "create_at" => time()
    ]);
  }

  // Mengambil detail dari data invoice
  $stmtdtl = $mssql->prepare("SELECT * FROM t_trx_license_invoice_dtl WHERE id_trx_lic_invoice = ?");
  $stmtdtl->execute(array($trxid));
  $datadtl = $stmtdtl->fetchAll();
  if($datadtl != NULL) {
    $log = $app->db->table("Log_V2")->insertGetId([
        "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "DETAIL INVOICE RETURN TRUE : ".$trxid, "create_at" => time()
    ]);
  } else {
    $log = $app->db->table("Log_V2")->insertGetId([
        "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "DETAIL INVOICE RETURN NULL : ".$trxid, "create_at" => time()
    ]);
  }

  // Mengambil ID User
  try {
    $userdtl = $mssql->prepare("SELECT Id FROM t_mst_user_login WHERE username = ?");
    // $userdtl = $mssql->prepare("SELECT CAST(Id AS TEXT) AS Id FROM t_mst_user_login WHERE username = ?");
    $userdtl->execute(array($data[0]["created_by"]));
    $udtl = $userdtl->fetchAll();
  } catch (PDOException $e) {
    $log = $app->db->table("Log_V2")->insertGetId([
        "type" => "INFO_LOG_MST_USER_LOGIN", "message" => $e->getMessage(), "create_at" => time()
    ]);
  }

  // Mengambil Email User
  try {
    $uedtl = $mssql->prepare("SELECT fullname, email_address FROM t_mst_user WHERE id_login = ?");
    $uedtl->execute(array(mssql_guid_string($udtl[0]["Id"])));
    $umdtl = $uedtl->fetchAll();
    // var_dump($umdtl);
    // echo $data[0]["created_by"];
  } catch (PDOException $e) {
    $log = $app->db->table("Log_V2")->insertGetId([
        "type" => "INFO_LOG_MST_USER", "message" => $e->getMessage(), "create_at" => time()
    ]);
  }

  $log = $app->db->table("Log_V2")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE MSSQL QUERY", "create_at" => time()
  ]);
  $expired_date = date("Y-m-d H:i:s", time()+172800);
  $sent_date = date("Y-m-d H:i:s", time());
  $payment = array(
  	"appsId" => "009",
  	"invoiceNo" => $data[0]["invoice_no"],
  	"routeId" => "001",
  	"data" => array(
        "PaymentHeader" => array(
          "TrxId" => "ANY-JK",
          "UserId" => "0",
        	"Password" => "0",
          "ExpiredDate" => $expired_date,
        	"DateSent" => $sent_date,
        	"KodeKL" => "022",
        	"KodeEselon1" => "05",
        	"KodeSatker" => "465632",
          "JenisPNBP" => "F",
          "KodeMataUang" => "1",
          "TotalNominalBilling" => $data[0]["total_price"],
          "NamaWajibBayar" => $data[0]["pic_name"],
        ),
        // PaymentDetails - Data for all transaction
        "PaymentDetails" => array(),
      ),

  );
  $total_billing = $data[0]["total_price"];
  function getPriceDetail($price_id, $mssql) {
    $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
    $prcdtl->execute(array($price_id));
    $pricedtl = $prcdtl->fetch(PDO::FETCH_OBJ);
    return $pricedtl;
  }
  $num = 0;
  foreach ($datadtl as $row) {
    $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
    $prcdtl->execute(array($row["id_mst_lic_price"]));
    $pricedtl = $prcdtl->fetchAll();

    foreach ($pricedtl as $prow) {
      $payment["data"]["PaymentDetails"]["PaymentDetail"][] = array(
          "NamaWajibBayar" => $row["nm_applicant"],
          "KodeTarifSimponi" => "00".$prow["price_code"],
          "KodePPSimponi" => "201615D",
          "KodeAkun" => "423216",
          "TarifPNBP" => $prow["amount"],
          "Volume" => $row["volume"],
          "Satuan" => $prow["lic_unit"],
          "TotalTarifPerRecord" => $prow["amount"] * $row["volume"],
        );
    }

    $num++;
  }

  // var_dump($payment);
  // die();
  // Merubah array yang diatas menjadi format JSON
  $paymentJson = json_encode($payment);
  $log = $app->db->table('Pre_Process')->insertGetId([
    'id_mssql' => $trxid, 'req_time' => time(), 'req_entity' => $paymentJson
  ]);
  // Mengirim ke fungsi SOA HubPayment
  function utf8_converter($array)
  {
    array_walk_recursive($array, function(&$item, $key){
      if(!mb_detect_encoding($item, 'utf-8', true)){
        $item = utf8_encode($item);
      }
    });

    return $array;
  }
  $res = $client->PaymentRequest(utf8_converter($payment));
  // Data yang diterima dari fungsi SOA HubPayment diconvert menjadi JSON
  // $res = json_encode($res);
  // Data yang tadi diterima sudah menjadi JSON lalu dirubah menjadi Array

  $data = json_decode(json_encode($res), true);
  $dataJson = json_encode($data);
  $log = $app->db->table("Log_V2")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CONT RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE SOAP FUNCTION AND DECODE", "create_at" => time()
  ]);

  $app->response->headers->set('Content-Type', 'application/json');
  
  // Jika response code sama dengan 00 ( sukses )
  if($data["response"]["code"] == "00") {
    // Simpan data pada database MySQL local API Payment sebagai arsip
    $log = $app->db->table('Trx_Record')->insertGetId([
      'id_mssql' => $trxid, 'req_time' => time(), 'req_entity' => $paymentJson, 'req_response' => $dataJson, 'req_response_code' => $data["response"]["code"], 'req_billingcode' => $data["response"]["simponiData"]["KodeBillingSimponi"], 'req_expired' => $data["response"]["simponiData"]["ExpiredDate"]
    ]);
    // Update data yang berada pada database MS SQL, memasukkan Billing Code
    try {
      $stmt = $mssql->prepare("UPDATE t_trx_license_invoice SET billing_code=?, expired_date=? WHERE id_trx_lic_invoice=?");
      $kodeBilling = $data["response"]["simponiData"]["KodeBillingSimponi"];
      $stmt->execute(array($kodeBilling,$expired_date,$trxid));
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    if ($umdtl[0]["email_address"] == null) {
      $mailaddr = "me@aap.my.id";
    } else {
      $mailaddr = $umdtl[0]["email_address"];
    }

    $url = "http://10.0.48.31/mail/".$umdtl[0]["fullname"]."/".$mailaddr."/".$data["response"]["simponiData"]["KodeBillingSimponi"]."/".$trxid."/".$total_billing;
    // $url = "http://10.0.48.31/mail/";
    // $log = $app->db->table('Log')->insert([
    //   'type' => 'INFO_LOG_EMAIL_PARAMS', 'message' => $url, 'create_at' => time(),
    // ]);

    $log = $app->db->table("Log_V2")->insertGetId([
        "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "MAIL RUNNING AT ".date("d-m-Y H:i:s").", WITH ".$url, "create_at" => time()
    ]);

    // $ch = curl_init();
    // curl_setopt($ch,CURLOPT_URL,$url);
    // curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    // // curl_setopt($ch,CURLOPT_HTTPHEADER,array('Origin: '.$_ip));
    // // $output = curl_exec($ch);
    // if (curl_exec($ch) === false) {
    //   echo 'Curl error: ' . curl_error($ch);
    // } else {
    //   echo 'Curl run perfectly';
    //   print_r($ch);
    // }
    //
    // curl_close($ch);

    // Mail Function

    $mail->isSMTP();
    $mail->Host = 'smtpgw.dephub.go.id';
    $mail->SMTPAuth = true;
    $mail->Username = 'admin.dkuppu@dephub.go.id';
    $mail->Password = 'dephub';

    $mail->Port = 25;

    $mail->setFrom('no-reply@dephub.go.id', 'PPO KEMENHUB');
    $mail->addAddress($mailaddr);

    // $mail->addCC('jismil.lathif@gmail.com');
    $mail->addBCC('syapril.yunus0104@gmail.com');
    // $mail->addCC('alvaryno@gmail.com');
    // $mail->addCC('rudianto@gmail.com');
    // $mail->addCC('great.wahyu@gmail.com');
    // $mail->addCC('sofian.hadie@gmail.com');
    $mail->addBCC('andri.praweda@gmail.com');
    // $mail->addCC('bowow@gmail.com');

    $mail->isHTML(true);

    $stmtdtl = $mssql->prepare("SELECT * FROM t_trx_license_invoice_dtl WHERE id_trx_lic_invoice = ?");
    $stmtdtl->execute(array($trxid));
    $datadtl = $stmtdtl->fetchAll();

    $template = file_get_contents('app/views/email_template.html');
    $detail_message = "";
    $no = 1;

    function service_name($sid, $mssql) {
      $stmt = $mssql->prepare("SELECT * FROM t_mst_license_price WHERE id_sub_license = ?");
      $stmt->execute(array($sid));
      $data = $stmt->fetchAll();
      return $data[0]["description"];
    }

    $invstmt = $mssql->prepare("SELECT * FROM t_trx_license_invoice WHERE id_trx_lic_invoice = ?");
    $invstmt->execute(array($trxid));
    $invdata = $invstmt->fetchAll();

    foreach ($datadtl as $row) {
      $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
      $prcdtl->execute(array($row["id_mst_lic_price"]));
      $pricedtl = $prcdtl->fetchAll();
      $nm_applicant = $row["nm_applicant"];
      foreach ($pricedtl as $prow) {
        $detail_message .= '<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">'.$no.'. '.$nm_applicant.' - '.$prow["description"].' - IDR. '.number_format($prow["amount"]).'</p>';
        $no++;
      }
    }
    $template = str_replace('%user_name%', $umdtl[0]["fullname"], $template);
    $template = str_replace('%data_detail%', $detail_message, $template);
    $template = str_replace('%invoice_no%', $invdata[0]["invoice_no"], $template);
    $template = str_replace('%billing_code%', $data["response"]["simponiData"]["KodeBillingSimponi"], $template);
    $template = str_replace('%billing_total%', number_format($total_billing), $template);
    $template = str_replace('%link_to_invoice%', 'http://pel.dephub.go.id:8010/invoice/'.$trxid, $template);
    // $template = str_replace('%link_to_invoice%', 'http://paygate.aap.my.id/dephub-invoice/'.$trxid, $template);

    $mail->Subject = '[DKPPU NOTIFICATION] Your request at DKPPU Online on '.date("d-m-Y H:i:s");
    $mail->MsgHTML($template);

    $log = $app->db->table('Log_V2')->insert([
      'type' => 'INFO_LOG_EMAIL', 'message' => 'PREPARE SEND TO '. $mailaddr .' W/ BC '. $data["response"]["simponiData"]["KodeBillingSimponi"], 'create_at' => time(),
    ]);
    if(!$mail->send()) {
        $log = $app->db->table('Log_V2')->insert([
          'type' => 'INFO_LOG_EMAIL', 'message' => 'FAILED TO '. $mailaddr .' W/ INFO '. $mail->ErrorInfo, 'create_at' => time(),
        ]);
    } else {
        $log = $app->db->table('Log_V2')->insertGetId([
          'type' => 'INFO_LOG_EMAIL', 'message' => 'SENT TO '. $mailaddr .' W/ BC '. $data["response"]["simponiData"]["KodeBillingSimponi"], 'create_at' => time()
        ]);
    }
    // End of Mail Function

    $return = array('result' => true, 'id_invoice' => $trxid, 'billing_code' => $kodeBilling);
    echo json_encode($return);
    $log = $app->db->table("Log_V2")->insertGetId([
        "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "PROGRESS COMPLETE FOR ID ".$trxid, "create_at" => time()
    ]);
  } else {
    // Jika response code bukan sama dengan 00 maka akan disimpan pada database MySQL untuk tindak lanjut
    $log = $app->db->table('Trx_Record')->insertGetId([
      'id_mssql' => $trxid, 'req_time' => time(), 'req_entity' => $paymentJson, 'req_response_code' => $data["response"]["code"]
    ]);
    $return = array('result' => false, 'message' => 'Pengajuan anda gagal');
    echo json_encode($return);
  }

});

$app->post('/api/v2/paymentrequest', function() use($app,$mssql,$mail) {
  header("access-control-allow-origin: *");

  $log = $app->db->table("Log_V2")->insertGetId([
    "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "ID RECEIVED ".$app->request->getBody(), "create_at" => time()
  ]);

  $trxid = $_POST["id"];

  $log = $app->db->table("Log_V2")->insertGetId([
    "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "REQUEST RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);

  // Inisiasi SOAP CLIENT
//   $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");
  $client = new SoapClient("http://10.47.210.17:7800/SimponiBRI_Service?wsdl");

  $log = $app->db->table("Log_V2")->insertGetId([
    "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "SOAP CONNECTED AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);

  $stmt = $mssql->prepare("SELECT * FROM t_trx_license_invoice WHERE id_trx_lic_invoice = ?");
  $stmt->execute(array($trxid));
  $data = $stmt->fetchAll();

  if ($data != NULL) {
    $log = $app->db->table("Log_V2")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "INVOICE RETURN TRUE : ".$trxid, "create_at" => time()
    ]);
  } else {
    $log = $app->db->table("Log_V2")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "INVOICE RETURN NULL : ".$trxid, "create_at" => time()
    ]);
  }

  // Mengambil detail dari data invoice
  $stmtdtl = $mssql->prepare("SELECT * FROM t_trx_license_invoice_dtl WHERE id_trx_lic_invoice = ?");
  $stmtdtl->execute(array($trxid));
  $datadtl = $stmtdtl->fetchAll();
  if($datadtl != NULL) {
    $log = $app->db->table("Log_V2")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "DETAIL INVOICE RETURN TRUE : ".$trxid, "create_at" => time()
    ]);
  } else {
    $log = $app->db->table("Log_V2")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "DETAIL INVOICE RETURN NULL : ".$trxid, "create_at" => time()
    ]);
  }

  // Mengambil ID User
  try {
    $userdtl = $mssql->prepare("SELECT Id FROM t_mst_user_login WHERE username = ?");
    $userdtl->execute(array($data[0]["created_by"]));
    $udtl = $userdtl->fetchAll();
  } catch (PDOException $e) {
    $log = $app->db->table("Log_V2")->insertGetId([
      "type" => "INFO_LOG_MST_USER_LOGIN", "message" => $e->getMessage(), "create_at" => time()
    ]);
  }

  try {
    $uedtl = $mssql->prepare("SELECT fullname, email_address FROM t_mst_user WHERE id_login = ?");
    $uedtl->execute(array(mssql_guid_string($udtl[0]["Id"])));
    $umdtl = $uedtl->fetchAll();
  } catch (PDOException $e) {
    $log = $app->db->table("Log_V2")->insertGetId([
      "type" => "INFO_LOG_MST_USER", "message" => $e->getMessage(), "create_at" => time()
    ]);
  }

  $log = $app->db->table("Log_V2")->insertGetId([
    "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE MSSQL QUERY", "create_at" => time()
  ]);
  $expired_date = date("Y-m-d H:i:s", time()+172800);
  $sent_date = date("Y-m-d H:i:s", time());
  $payment = array(
    "appsId" => "009",
    "invoiceNo" => $data[0]["invoice_no"],
    "routeId" => "001",
    "data" => array(
      "PaymentHeader" => array(
        "TrxId" => "ANY-JK",
        "UserId" => "0",
        "Password" => "0",
        "ExpiredDate" => $expired_date,
        "DateSent" => $sent_date,
        "KodeKL" => "022",
        "KodeEselon1" => "05",
        "KodeSatker" => "465632",
        "JenisPNBP" => "F",
        "KodeMataUang" => "1",
        "TotalNominalBilling" => $data[0]["total_price"],
        "NamaWajibBayar" => $data[0]["pic_name"],
      ),
      "PaymentDetails" => array(),
    ),

  );
  $total_billing = $data[0]["total_price"];
  function getPriceDetail($price_id, $mssql) {
    $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
    $prcdtl->execute(array($price_id));
    $pricedtl = $prcdtl->fetch(PDO::FETCH_OBJ);
    return $pricedtl;
  }
  $num = 0;
  foreach ($datadtl as $row) {
    $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
    $prcdtl->execute(array($row["id_mst_lic_price"]));
    $pricedtl = $prcdtl->fetchAll();

    foreach ($pricedtl as $prow) {
      $payment["data"]["PaymentDetails"]["PaymentDetail"][] = array(
        "NamaWajibBayar" => $row["nm_applicant"],
        "KodeTarifSimponi" => "00".$prow["price_code"],
        "KodePPSimponi" => "201615D",
        "KodeAkun" => "423216",
        "TarifPNBP" => $prow["amount"],
        "Volume" => $row["volume"],
        "Satuan" => $prow["lic_unit"],
        "TotalTarifPerRecord" => $prow["amount"] * $row["volume"],
      );
    }

    $num++;
  }

  $paymentJson = json_encode($payment);
  $log = $app->db->table('Pre_Process')->insertGetId([
    'id_mssql' => $trxid, 'req_time' => time(), 'req_entity' => $paymentJson
  ]);

  function utf8_converter($array)
  {
    array_walk_recursive($array, function(&$item, $key){
      if(!mb_detect_encoding($item, 'utf-8', true)){
        $item = utf8_encode($item);
      }
    });

    return $array;
  }
//  utf8_converter($payment);

  $res = $client->PaymentRequest(utf8_converter($payment));

  $data = json_decode(json_encode($res), true);

  var_dump(json_encode($res));
  die();


  $dataJson = json_encode($data);
  $log = $app->db->table("Log_V2")->insertGetId([
    "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CONT RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE SOAP FUNCTION AND DECODE", "create_at" => time()
  ]);

  $app->response->headers->set('Content-Type', 'application/json');

  // Jika response code sama dengan 00 ( sukses )
  if($data["response"]["code"] == "00") {
    // Simpan data pada database MySQL local API Payment sebagai arsip
    $log = $app->db->table('Trx_Record')->insertGetId([
      'id_mssql' => $trxid, 'req_time' => time(), 'req_entity' => $paymentJson, 'req_response' => $dataJson, 'req_response_code' => $data["response"]["code"], 'req_billingcode' => $data["response"]["simponiData"]["KodeBillingSimponi"], 'req_expired' => $data["response"]["simponiData"]["ExpiredDate"]
    ]);
    // Update data yang berada pada database MS SQL, memasukkan Billing Code
    try {
      $stmt = $mssql->prepare("UPDATE t_trx_license_invoice SET billing_code=?, expired_date=? WHERE id_trx_lic_invoice=?");
      $kodeBilling = $data["response"]["simponiData"]["KodeBillingSimponi"];
      $stmt->execute(array($kodeBilling,$expired_date,$trxid));
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    if ($umdtl[0]["email_address"] == null) {
      $mailaddr = "me@aap.my.id";
    } else {
      $mailaddr = $umdtl[0]["email_address"];
    }

    $url = "http://10.0.48.31/mail/".$umdtl[0]["fullname"]."/".$mailaddr."/".$data["response"]["simponiData"]["KodeBillingSimponi"]."/".$trxid."/".$total_billing;
    // $url = "http://10.0.48.31/mail/";
    // $log = $app->db->table('Log')->insert([
    //   'type' => 'INFO_LOG_EMAIL_PARAMS', 'message' => $url, 'create_at' => time(),
    // ]);

    $log = $app->db->table("Log_V2")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "MAIL RUNNING AT ".date("d-m-Y H:i:s").", WITH ".$url, "create_at" => time()
    ]);

    // $ch = curl_init();
    // curl_setopt($ch,CURLOPT_URL,$url);
    // curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    // // curl_setopt($ch,CURLOPT_HTTPHEADER,array('Origin: '.$_ip));
    // // $output = curl_exec($ch);
    // if (curl_exec($ch) === false) {
    //   echo 'Curl error: ' . curl_error($ch);
    // } else {
    //   echo 'Curl run perfectly';
    //   print_r($ch);
    // }
    //
    // curl_close($ch);

    // Mail Function

    $mail->isSMTP();
    $mail->Host = 'smtpgw.dephub.go.id';
    $mail->SMTPAuth = true;
    $mail->Username = 'admin.dkuppu@dephub.go.id';
    $mail->Password = 'dephub';

    $mail->Port = 25;

    $mail->setFrom('no-reply@dephub.go.id', 'PPO KEMENHUB');
    $mail->addAddress($mailaddr);

    // $mail->addCC('jismil.lathif@gmail.com');
    $mail->addBCC('syapril.yunus0104@gmail.com');
    // $mail->addCC('alvaryno@gmail.com');
    // $mail->addCC('rudianto@gmail.com');
    // $mail->addCC('great.wahyu@gmail.com');
    // $mail->addCC('sofian.hadie@gmail.com');
    $mail->addBCC('andri.praweda@gmail.com');
    // $mail->addCC('bowow@gmail.com');

    $mail->isHTML(true);

    $stmtdtl = $mssql->prepare("SELECT * FROM t_trx_license_invoice_dtl WHERE id_trx_lic_invoice = ?");
    $stmtdtl->execute(array($trxid));
    $datadtl = $stmtdtl->fetchAll();

    $template = file_get_contents('app/views/email_template.html');
    $detail_message = "";
    $no = 1;

    function service_name($sid, $mssql) {
      $stmt = $mssql->prepare("SELECT * FROM t_mst_license_price WHERE id_sub_license = ?");
      $stmt->execute(array($sid));
      $data = $stmt->fetchAll();
      return $data[0]["description"];
    }

    $invstmt = $mssql->prepare("SELECT * FROM t_trx_license_invoice WHERE id_trx_lic_invoice = ?");
    $invstmt->execute(array($trxid));
    $invdata = $invstmt->fetchAll();

    foreach ($datadtl as $row) {
      $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
      $prcdtl->execute(array($row["id_mst_lic_price"]));
      $pricedtl = $prcdtl->fetchAll();
      $nm_applicant = $row["nm_applicant"];
      foreach ($pricedtl as $prow) {
        $detail_message .= '<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">'.$no.'. '.$nm_applicant.' - '.$prow["description"].' - IDR. '.number_format($prow["amount"]).'</p>';
        $no++;
      }
    }
    $template = str_replace('%user_name%', $umdtl[0]["fullname"], $template);
    $template = str_replace('%data_detail%', $detail_message, $template);
    $template = str_replace('%invoice_no%', $invdata[0]["invoice_no"], $template);
    $template = str_replace('%billing_code%', $data["response"]["simponiData"]["KodeBillingSimponi"], $template);
    $template = str_replace('%billing_total%', number_format($total_billing), $template);
    $template = str_replace('%link_to_invoice%', 'http://pel.dephub.go.id:8010/invoice/'.$trxid, $template);
    // $template = str_replace('%link_to_invoice%', 'http://paygate.aap.my.id/dephub-invoice/'.$trxid, $template);

    $mail->Subject = '[DKPPU NOTIFICATION] Your request at DKPPU Online on '.date("d-m-Y H:i:s");
    $mail->MsgHTML($template);

    $log = $app->db->table('Log_V2')->insert([
      'type' => 'INFO_LOG_EMAIL', 'message' => 'PREPARE SEND TO '. $mailaddr .' W/ BC '. $data["response"]["simponiData"]["KodeBillingSimponi"], 'create_at' => time(),
    ]);
    if(!$mail->send()) {
      $log = $app->db->table('Log_V2')->insert([
        'type' => 'INFO_LOG_EMAIL', 'message' => 'FAILED TO '. $mailaddr .' W/ INFO '. $mail->ErrorInfo, 'create_at' => time(),
      ]);
    } else {
      $log = $app->db->table('Log_V2')->insertGetId([
        'type' => 'INFO_LOG_EMAIL', 'message' => 'SENT TO '. $mailaddr .' W/ BC '. $data["response"]["simponiData"]["KodeBillingSimponi"], 'create_at' => time()
      ]);
    }
    // End of Mail Function

    $return = array('result' => true, 'id_invoice' => $trxid, 'billing_code' => $kodeBilling);
    echo json_encode($return);
    $log = $app->db->table("Log_V2")->insertGetId([
      "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "PROGRESS COMPLETE FOR ID ".$trxid, "create_at" => time()
    ]);
  } else {
    // Jika response code bukan sama dengan 00 maka akan disimpan pada database MySQL untuk tindak lanjut
    $log = $app->db->table('Trx_Record')->insertGetId([
      'id_mssql' => $trxid, 'req_time' => time(), 'req_entity' => $paymentJson, 'req_response_code' => $data["response"]["code"]
    ]);
    $return = array('result' => false, 'message' => 'Pengajuan anda gagal');
    echo json_encode($return);
  }

});

$app->post('/pyrq', function() use($app, $mssql) {
  header("access-control-allow-origin: *");
  // $trxid = $app->request->post("id");
  // $_ip = $_SERVER["HTTP_ORIGIN"];
  // echo($_ip);
  // die();

  $log = $app->db->table("Log")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "ID RECEIVED ".$app->request->getBody(), "create_at" => time()
  ]);

  $trxid = $_POST["id"];

  $log = $app->db->table("Log")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "REQUEST RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);

  // Inisiasi SOAP CLIENT
  // $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");

  $log = $app->db->table("Log")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "SOAP CONNECTED AT ".date("d-m-Y H:i:s"), "create_at" => time()
  ]);
  // Mengambil data yang akan dikirim ke SOA HubPayment
  $stmt = $mssql->prepare("SELECT * FROM t_trx_license_invoice WHERE id_trx_lic_invoice = ?");
  $stmt->execute(array($trxid));
  $data = $stmt->fetchAll();
  if ($data != NULL) {
    $log = $app->db->table("Log")->insertGetId([
        "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "INVOICE RETURN TRUE : ".$trxid, "create_at" => time()
    ]);
  } else {
    $log = $app->db->table("Log")->insertGetId([
        "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "INVOICE RETURN NULL : ".$trxid, "create_at" => time()
    ]);
  }

  // Mengambil detail dari data invoice
  $stmtdtl = $mssql->prepare("SELECT * FROM t_trx_license_invoice_dtl WHERE id_trx_lic_invoice = ?");
  $stmtdtl->execute(array($trxid));
  $datadtl = $stmtdtl->fetchAll();
  if($datadtl != NULL) {
    $log = $app->db->table("Log")->insertGetId([
        "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "DETAIL INVOICE RETURN TRUE : ".$trxid, "create_at" => time()
    ]);
  } else {
    $log = $app->db->table("Log")->insertGetId([
        "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "DETAIL INVOICE RETURN NULL : ".$trxid, "create_at" => time()
    ]);
  }

  // Mengambil ID User
  try {
    $userdtl = $mssql->prepare("SELECT Id FROM t_mst_user_login WHERE username = ?");
    // $userdtl = $mssql->prepare("SELECT CAST(Id AS TEXT) AS Id FROM t_mst_user_login WHERE username = ?");
    $userdtl->execute(array($data[0]["created_by"]));
    $udtl = $userdtl->fetchAll();
  } catch (PDOException $e) {
    $log = $app->db->table("Log_DB")->insertGetId([
        "type" => "INFO_LOG_MST_USER_LOGIN", "message" => $e->getMessage(), "create_at" => time()
    ]);
  }

  // Mengambil Email User
  try {
    $uedtl = $mssql->prepare("SELECT fullname, email_address FROM t_mst_user WHERE id_login = ?");
    $uedtl->execute(array(mssql_guid_string($udtl[0]["Id"])));
    $umdtl = $uedtl->fetchAll();
    // var_dump($umdtl);
    // echo $data[0]["created_by"];
  } catch (PDOException $e) {
    $log = $app->db->table("Log_DB")->insertGetId([
        "type" => "INFO_LOG_MST_USER", "message" => $e->getMessage(), "create_at" => time()
    ]);
  }

  $log = $app->db->table("Log")->insertGetId([
      "ip_origin" => "", "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE MSSQL QUERY", "create_at" => time()
  ]);
  $expired_date = date("Y-m-d H:i:s", time()+151200);
  $sent_date = date("Y-m-d H:i:s", time());
  $payment = array(
  	"appsId" => "009",
  	"invoiceNo" => $data[0]["invoice_no"],
  	"routeId" => "001",
  	"data" => array(
        "PaymentHeader" => array(
          "TrxId" => "ANY-JK",
          "UserId" => "0",
        	"Password" => "0",
          "ExpiredDate" => $expired_date,
        	"DateSent" => $sent_date,
        	"KodeKL" => "022",
        	"KodeEselon1" => "05",
        	"KodeSatker" => "465632",
          "JenisPNBP" => "F",
          "KodeMataUang" => "1",
          "TotalNominalBilling" => $data[0]["total_price"],
          "NamaWajibBayar" => $data[0]["pic_name"],
        ),
        // PaymentDetails - Data for all transaction
        "PaymentDetails" => array(),
      ),

  );
  $total_billing = $data[0]["total_price"];
  function getPriceDetail($price_id, $mssql) {
    $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
    $prcdtl->execute(array($price_id));
    $pricedtl = $prcdtl->fetch(PDO::FETCH_OBJ);
    return $pricedtl;
  }
  $num = 0;
  foreach ($datadtl as $row) {
    $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
    $prcdtl->execute(array($row["id_mst_lic_price"]));
    $pricedtl = $prcdtl->fetchAll();

    foreach ($pricedtl as $prow) {
      $payment["data"]["PaymentDetails"]["PaymentDetail"][] = array(
          "NamaWajibBayar" => $row["nm_applicant"],
          "KodeTarifSimponi" => "00".$prow["price_code"],
          "KodePPSimponi" => "2015011",
          "KodeAkun" => "423216",
          "TarifPNBP" => $prow["amount"],
          "Volume" => $row["volume"],
          "Satuan" => $prow["lic_unit"],
          "TotalTarifPerRecord" => $prow["amount"] * $row["volume"],
        );
    }

    $num++;
  }

  // Merubah array yang diatas menjadi format JSON
  // $paymentJson = json_encode($payment);
  // Mengirim ke fungsi SOA HubPayment
  // $res = $client->PaymentRequest($payment);
  $log = $app->db->table("Log")->insertGetId([
      "type" => "CUSTOM_ERROR_NEED", "message" => $payment, "create_at" => time()
  ]);
  die();
  // Data yang diterima dari fungsi SOA HubPayment diconvert menjadi JSON
  // $res = json_encode($res);
  // Data yang tadi diterima sudah menjadi JSON lalu dirubah menjadi Array

  // $data = json_decode(json_encode($res), true);
  // $dataJson = json_encode($data);
  // $log = $app->db->table("Log")->insertGetId([
  //     "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "CONT RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE SOAP FUNCTION AND DECODE", "create_at" => time()
  // ]);
  //
  // $app->response->headers->set('Content-Type', 'application/json');
  // // print_r($res);
  //
  // // Jika response code sama dengan 00 ( sukses )
  // if($data["response"]["code"] == "00") {
  //   // Simpan data pada database MySQL local API Payment sebagai arsip
  //   $log = $app->db->table('Trx_Record')->insertGetId([
  //     'id_mssql' => $trxid, 'req_time' => time(), 'req_entity' => $paymentJson, 'req_response' => $dataJson, 'req_response_code' => $data["response"]["code"], 'req_billingcode' => $data["response"]["simponiData"]["KodeBillingSimponi"], 'req_expired' => $data["response"]["simponiData"]["ExpiredDate"]
  //   ]);
  //   // Update data yang berada pada database MS SQL, memasukkan Billing Code
  //   try {
  //     $stmt = $mssql->prepare("UPDATE t_trx_license_invoice SET billing_code=?, expired_date=? WHERE id_trx_lic_invoice=?");
  //     $kodeBilling = $data["response"]["simponiData"]["KodeBillingSimponi"];
  //     $stmt->execute(array($kodeBilling,$expired_date,$trxid));
  //   } catch (PDOException $e) {
  //     echo $e->getMessage();
  //   }
  //
  //   if ($umdtl[0]["email_address"] == null) {
  //     $mailaddr = "me@aap.my.id";
  //   } else {
  //     $mailaddr = $umdtl[0]["email_address"];
  //   }
  //
  //   $url = "http://10.0.48.31/mail/".$umdtl[0]["fullname"]."/".$mailaddr."/".$data["response"]["simponiData"]["KodeBillingSimponi"]."/".$trxid."/".$total_billing;
  //   // $url = "http://10.0.48.31/mail/";
  //   // $log = $app->db->table('Log')->insert([
  //   //   'type' => 'INFO_LOG_EMAIL_PARAMS', 'message' => $url, 'create_at' => time(),
  //   // ]);
  //
  //   $log = $app->db->table("Log")->insertGetId([
  //       "type" => "INFO_LOG_PAYMENT_INPUT", "message" => "MAIL RUNNING AT ".date("d-m-Y H:i:s").", WITH ".$url, "create_at" => time()
  //   ]);
  //
  //   $ch = curl_init();
  //   curl_setopt($ch,CURLOPT_URL,$url);
  //   curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  //   // curl_setopt($ch,CURLOPT_HTTPHEADER,array('Origin: '.$_ip));
  //   $output = curl_exec($ch);
  //   curl_close($ch);
  //
  //   $return = array('result' => true, 'id_invoice' => $trxid, 'billing_code' => $kodeBilling);
  //   echo json_encode($return);
  //
  // } else {
  //   // Jika response code bukan sama dengan 00 maka akan disimpan pada database MySQL untuk tindak lanjut
  //   $log = $app->db->table('Trx_Record')->insertGetId([
  //     'id_mssql' => $trxid, 'req_time' => time(), 'req_entity' => $paymentJson, 'req_response_code' => $data["response"]["code"]
  //   ]);
  //   $return = array('result' => false, 'message' => 'Pengajuan anda gagal');
  //   echo json_encode($return);
  // }
});

$app->map('/checker', function() use($app, $mssql) {
  // $get = $app->db->table('Trx_Record')->where("id", 86006)->first();
  // var_dump($get);
  // $data = json_decode($get["req_billing_response"], true);
  // var_dump($data["ResponseData"]["NTB"]);
})->via('GET', 'POST');

$app->get('/sn/:id', function($id) use ($app, $mssql) {

  if(strlen($id) === 6) {
    $id = substr($id, 2);
  }
  function service_name($sid, $mssql) {
    $stmt = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE price_code = ?");
    $stmt->execute(array($sid));
    $data = $stmt->fetchAll();
    return $data[0]["description"];
  }
  echo service_name($id, $mssql);
});
// Mail Handler
// $app->get('/mail/:orgname/:mailaddr/:billingcode/:invid/:total', function($orgname, $mailaddr, $billingcode, $invid, $total) use($app, $mail, $mssql) {
//
//   if ($mailaddr == null) {
//     $mailaddr = "me@aap.my.id";
//   }
//   $mail->isSMTP();
//   $mail->Host = 'smtpgw.dephub.go.id';
//   $mail->SMTPAuth = true;
//   $mail->Username = 'admin.dkuppu@dephub.go.id';
//   $mail->Password = 'dephub';
//
//   $mail->Port = 25;
//
//   $mail->setFrom('no-reply@dephub.go.id', 'PPO DEPHUB');
//   $mail->addAddress($mailaddr);
//
//   $mail->addCC('jismil.lathif@gmail.com');
//   $mail->addCC('syapril.yunus0104@gmail.com');
//   $mail->addCC('alvaryno@gmail.com');
//   $mail->addCC('rudianto@gmail.com');
//   $mail->addCC('great.wahyu@gmail.com');
//   $mail->addCC('sofian.hadie@gmail.com');
//   $mail->addCC('andriaripratama@gmail.com');
//   $mail->addCC('bowow@gmail.com');
//
//   $mail->isHTML(true);
//
//   $stmtdtl = $mssql->prepare("SELECT * FROM t_trx_license_invoice_dtl WHERE id_trx_lic_invoice = ?");
//   $stmtdtl->execute(array($invid));
//   $datadtl = $stmtdtl->fetchAll();
//
//   $template = file_get_contents('app/views/email_template.html');
//   $detail_message = "";
//   $no = 1;
//
//   function service_name($sid, $mssql) {
//     $stmt = $mssql->prepare("SELECT * FROM t_mst_license_price WHERE id_sub_license = ?");
//     $stmt->execute(array($sid));
//     $data = $stmt->fetchAll();
//     return $data[0]["description"];
//   }
//
//   $invstmt = $mssql->prepare("SELECT * FROM t_trx_license_invoice WHERE id_trx_lic_invoice = ?");
//   $invstmt->execute(array($invid));
//   $invdata = $invstmt->fetchAll();
//
//   foreach ($datadtl as $row) {
//     $prcdtl = $mssql->prepare("SELECT * FROM t_mst_license_price_dtl WHERE id_mst_lic_price = ?");
//     $prcdtl->execute(array($row["id_mst_lic_price"]));
//     $pricedtl = $prcdtl->fetchAll();
//     $nm_applicant = $row["nm_applicant"];
//     foreach ($pricedtl as $prow) {
//       $detail_message .= '<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">'.$no.'. '.$nm_applicant.' - '.$prow["description"].' - IDR. '.number_format($prow["amount"]).'</p>';
//       $no++;
//     }
//   }
//   $template = str_replace('%user_name%', $orgname, $template);
//   $template = str_replace('%data_detail%', $detail_message, $template);
//   $template = str_replace('%invoice_no%', $invdata[0]["invoice_no"], $template);
//   $template = str_replace('%billing_code%', $billingcode, $template);
//   $template = str_replace('%billing_total%', number_format($total), $template);
//   // $template = str_replace('%link_to_invoice%', 'http://36.66.76.51:8010/invoice/'.$invid, $template);
//   $template = str_replace('%link_to_invoice%', 'http://paygate.aap.my.id/dephub-invoice/'.$invid, $template);
//
//   $mail->Subject = '[DKPPU NOTIFICATION] Your request at DKPPU Online';
//   $mail->MsgHTML($template);
//   // $mail->Body    = 'Yth. '.$orgname.',<br><br>Kami menerima permintaan anda atas pengajuan penerbitan lisensi baru, sebelum dapat diproses harap melakukan pembayaran dengan Kode Billing <strong>'.$billingcode.'</strong> pada Bank BRI.<br><br>Hormat Kami,<br><br>PPO Dephub';
//   // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
//   $log = $app->db->table('Log')->insert([
//     'type' => 'INFO_LOG_EMAIL', 'message' => 'PREPARE SEND TO '. $mailaddr .' W/ BC '. $billingcode, 'create_at' => time(),
//   ]);
//   if(!$mail->send()) {
//       $log = $app->db->table('Log')->insert([
//         'type' => 'INFO_LOG_EMAIL', 'message' => 'FAILED TO '. $mailaddr .' W/ INFO '. $mail->ErrorInfo, 'create_at' => time(),
//       ]);
//   } else {
//       $log = $app->db->table('Log')->insertGetId([
//         'type' => 'INFO_LOG_EMAIL', 'message' => 'SENT TO '. $mailaddr .' W/ BC '. $billingcode, 'create_at' => time()
//       ]);
//       return true;
//   }
//
// });

$app->get('/payinterface', function() use($app) {
    $app->render('payinterface.html');
});
$app->get('/backend', function() use($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->render('backend_index.html');
  } else {
    $data = $app->db->table('Trx_Record')->get();
    $data_void = $app->db->table('Trx_Record')->where("req_response_code", "!=", "00")->orderBy("id","desc")->get();
    $data_success = $app->db->table('Trx_Record')->where("req_response_code", "00")->orderBy("id","desc")->get();
    $data_paid = $app->db->table('Trx_Record')->where("req_paid", "1")->orderBy("id","desc")->get();
    $data_nonpaid = $app->db->table('Trx_Record')->where("req_paid", "0")->orderBy("id","desc")->get();
    $params = array('data_records' => count($data), 'data_void' => count($data_void), 'data_success' => count($data_success), 'data_paid' => count($data_paid), 'data_nonpaid' => count($data_nonpaid), );
    $app->render('backend_index_logged.html', $params);
  }

});
$app->post('/auth', function() use($app) {
  $user = $app->request->post('user');
  $pass = $app->request->post('pass');

  $check = $app->db->table('backend_user')->where('user', $user)->first();

  if($check == NULL) {
    echo "User tidak terdaftar";
  }

  $passdb = $check["pass"];

  if (hash('sha256',$pass) == $passdb) {
    $_SESSION["logged_user"] = $user;
    $app->redirect('/backend');
  } else {
    echo "Password tidak valid";
  }
});
$app->get('/backend/log', function() use ($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->redirect('/backend');
  }

  $data = $app->db->table('Trx_Record')->where("req_response_code", "00")->orderBy("id","desc")->get();

  foreach ($data as $row) {

    $rejd = json_decode($row["req_entity"], true);
    $rrjd = json_decode($row["req_response"], true);
    $ds[] = array("id" => $row["id"], "reqTime" => $row["req_time"], "reqEntity" => $rejd, "reqResponse" => $rrjd,);
  }

  // var_dump(json_encode($ds));

  $app->render('backend_log.html', array('data' => $ds));
});
$app->get('/backend/log/paid', function() use ($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->redirect('/backend');
  }

  $data = $app->db->table('Trx_Record')->where("req_paid", "1")->orderBy("id","desc")->get();

  foreach ($data as $row) {

    $rejd = json_decode($row["req_entity"], true);
    $rrjd = json_decode($row["req_response"], true);
    $ds[] = array("id" => $row["id"], "reqTime" => $row["req_time"], "reqEntity" => $rejd, "reqResponse" => $rrjd,);
  }
  $app->render('backend_log_paid.html', array('data' => $ds));
});
$app->get('/backend/log/non-paid', function() use ($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->redirect('/backend');
  }

  $data = $app->db->table('Trx_Record')->where("req_paid", "0")->orderBy("id","desc")->get();

  foreach ($data as $row) {

    $rejd = json_decode($row["req_entity"], true);
    $rrjd = json_decode($row["req_response"], true);
    $ds[] = array("id" => $row["id"], "reqTime" => $row["req_time"], "reqEntity" => $rejd, "reqResponse" => $rrjd,);
  }
  $app->render('backend_log_nonpaid.html', array('data' => $ds));
});
$app->get('/backend/log/fail', function() use ($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->redirect('/backend');
  }

  $data = $app->db->table('Trx_Record')->where("req_response_code", "!=", "00")->orderBy("id","desc")->get();

  foreach ($data as $row) {

    $rejd = json_decode($row["req_entity"], true);
    $rrjd = json_decode($row["req_response"], true);
    $ds[] = array("id" => $row["id"], "reqTime" => $row["req_time"], "reqEntity" => $rejd, "reqResponse" => $rrjd, "req_response_code" => $row["req_response_code"], );
  }

  // var_dump(json_encode($ds));

  $app->render('backend_log_fail.html', array('data' => $ds));
});
$app->get('/backend/log/range', function() use ($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->redirect('/backend');
  }

  $app->render('backend_log_range.html');
});
$app->post('/backend/log/range', function() use ($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->redirect('/backend');
  }
  $start_date = $app->request->post('start_date');
  $end_date = $app->request->post('end_date');

  $whereState = "req_time >= ".strtotime($start_date)." AND req_time <=".strtotime($end_date);
  $data = $app->db->table("Trx_Record")->whereRaw($whereState)->get();

  // var_dump($data);
  foreach ($data as $row) {

    $rejd = json_decode($row["req_entity"], true);
    $rrjd = json_decode($row["req_response"], true);
    $ds[] = array("id" => $row["id"], "reqTime" => $row["req_time"], "reqEntity" => $rejd, "reqResponse" => $rrjd,);
  }
  $app->render('backend_log_range.html', array('data' => $ds));
});
$app->get('/backend/doc', function() use ($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->redirect('/backend');
  }

  $app->render('backend_doc_get_billing.html');
});
$app->get('/backend/log/:id', function($id) use ($app) {
  if (empty($_SESSION["logged_user"])) {
    $app->redirect('/backend');
  }

  $data = $app->db->table('Trx_Record')->where("id", $id)->first();

  $ds["data"] = array("id" => $data["id"], "id_mssql" => $data["id_mssql"], "reqTime" => $data["req_time"], "reqEntity" => json_decode($data["req_entity"], true), "reqBillingResponse" => json_decode($data["req_billing_response"], true), "reqPaid" => $data["req_paid"], "reqNtb" => $data["req_ntb"], "reqNtpn" => $data["req_ntpn"], );
  $app->render('backend_log_view.html', $ds);
});
$app->get('/backend/checker', function() use ($app) {

});
$app->post('/backend/checker', function() use ($app) {

});
$app->get('/backend/logout', function() use ($app) {
  session_unset();
  session_destroy();
  $app->redirect('/backend');
});
$app->get('/backend/sha/:key', function($key) {
  echo hash('sha256', $key);
});
$app->post('/testcurl', function() use($app) {
  header("access-control-allow-origin: *");
  $_ip = $_SERVER["HTTP_ORIGIN"];
  // var_dump($_SERVER);
  // die();
  $header = array('Origin' => $_ip, );
  $url = "http://10.0.48.31/mail/User Aero Flyer Institute No.01/rizkhapramesti@gmail.com/820160126316903/EE2EF606-3DAF-42D8-8B88-ACEC84E430C3/300000";
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch,CURLOPT_URL,$url);
  $output = curl_exec($ch);

  if(curl_errno($ch)) {
    echo 'error:' . curl_error($ch);
  }

  curl_close($ch);


});

$app->get('/rekap', function() use($app) {
  //$data = $app->db->table('Trx_Record')->where("req_time","<",1483201403)->where("req_paid", "1")->orderBy("id","desc")->get();
//  July to December
  $data = $app->db->table('Trx_Record')->where("req_time","<",1483201403)->where("req_paid", "1")->orderBy("id","desc")->get();
  $total = 0;
  foreach ($data as $row) {

    $rrpo = json_decode($row["req_response"], true);
    $rrjd = json_decode($row["req_billing_response"], true);
//    $ds[] = array("id" => $row["id"], "reqTime" => $row["req_time"], "reqEntity" => $rejd, "reqResponse" => $rrjd,);
//    $total += $rrjd["data"]["PaymentHeader"]["TotalNominalBilling"];
    
    $app->db->table("trx_rekap")->insert([
      "trx_date" => date("Y-m-d", $row["req_time"]),
      "trx_date_settle" => date("Y-m-d", strtotime($rrjd["ResponseData"]["TrxDate"])),
      "trx_invoice" => $rrjd["RequestData"]["invoiceNo"],
      "trx_amount" => $rrpo["data"]["PaymentHeader"]["TotalNominalBilling"],
      "trx_simponi_code" => $rrjd["RequestData"]["KodeBillingSimponi"],
      "trx_ntb" => $rrjd["ResponseData"]["NTB"],
      "trx_ntpn" => $rrjd["ResponseData"]["NTPN"],
    ]);
  }

//  echo number_format($total);
});

$app->get('/bsrq/:billing', function($billing) use ($app, $mssql) {
  // $log = $app->db->table("Log")->insertGetId([
  //     "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "CRON BILLING CHECKER RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  // ]);
//  $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl", array('cache_wsdl' => WSDL_CACHE_NONE, 'features'=>SOAP_SINGLE_ELEMENT_ARRAYS));
  $client = new SoapClient("http://10.47.210.17:7800/SimponiBRI_Service?wsdl");
  // $log = $app->db->table("Log")->insertGetId([
  //     "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "CRON BILLING CHECKER SOAP CONNECTED & RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
  // ]);
  // $getdata = $app->db->table('Trx_Record')->where("req_billingcode", "820160108315455")->get();
  $getdata = $app->db->table('Trx_Record')->where("req_billingcode", $billing)->orderBy("id", "desc")->get();
  // var_dump($getdata);
  // die();
  if ($getdata != NULL) {
    foreach ($getdata as $r) {
      $data = $r["req_response"];
      $dataConvert = (array) json_decode($data);

      $payment = array(
      	"appsId" => "009",
      	"invoiceNo" => $dataConvert["invoiceNo"],
      	"routeId" => "001",
      	"TrxId" => "ANY-JK",
      	"UserId" => "0",
      	"Password" => "0",
      	"KodeBillingSimponi" => $r["req_billingcode"],
      	"KodeKL" => "022",
      	"KodeEselon1" => "05",
      	"KodeSatker" => "465632",
      );
      $res = $client->BillingStatus($payment);
      $res = json_encode($res);
      $data = json_decode($res, true);
      var_dump($res);
      if($data["ResponseCode"] === "00") {
        $update = $app->db->table("Trx_Record")->where("req_billingcode", $r["req_billingcode"])->update(array('req_paid' => 1, 'req_billing_response' => $res,));
        // echo "Pay<br>";
        try {
          $stmt = $mssql->prepare("UPDATE t_trx_license_invoice SET is_paid=?, ntb=?, ntpn=?, paid_date=? WHERE billing_code=?");
          $is_paid = 1;
          $billing_code = $r["req_billingcode"];
          $ntb = $data["ResponseData"]["NTB"];
          $ntpn = $data["ResponseData"]["NTPN"];
          $trxdate = $data["ResponseData"]["TrxDate"];
          $stmt->execute(array($is_paid,$ntb,$ntpn,$trxdate,$billing_code));
        } catch (PDOException $e) {
          // echo $e->getMessage();
          $log = $app->db->table("Log")->insertGetId([
            "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "ERROR OF MSSQL : ".$e->getMessage(), "create_at" => time()
          ]);
        }
      } else {
        // echo "No Pay yet<br>";
      }
    }
  } else {
    $log = $app->db->table("Log")->insertGetId([
        "type" => "INFO_LOG_PAYMENT_CHECKER", "message" => "NO NEW DATA", "create_at" => time()
    ]);
    die();
  }
});

// Air Crew License Check Mobile App
// API
// Version 1.0

$app->post('/api/licCheck', function() use ($app, $mssql) {
  $licNo = $_POST["lic_number"];

  $stmt = $mssql->prepare("SELECT
ma.id_applicant,
ma.fullname,
ma.photo_file,
UPPER(ma.place_of_birth) + ' '+CONVERT(varchar(20),convert(varchar, ma.birth_date, 103)) as placeandbirth,
ma.height,
ma.weight,
(SELECT ml.lookup_value FROM t_mst_lookup ml WHERE ml.id_lookup = ma.id_hair_color
) as hair_type_color,
(SELECT ml.lookup_value FROM t_mst_lookup ml WHERE ml.id_lookup = ma.id_eyes_color
) as eye_type_color,
(SELECT ml.lookup_value FROM t_mst_lookup ml WHERE ml.id_lookup = ma.id_gender
) as sex_code,
(SELECT mlc.country_name
FROM t_mst_loc_country mlc
WHERE mlc.id_country = ma.id_nationality
) AS nationality,
ma.address + ' RT:' + ma.address_rt + ' RW:' + ma.address_rw + ' ' +
(SELECT lv.village_name FROM t_mst_loc_village lv WHERE lv.id_village = ma.id_village) 
+ ' '+ 
(SELECT ls.subdist_name FROM t_mst_loc_subdistrict ls WHERE ls.id_subdist = ma.id_subdist)
+' '+
(SELECT ld.district_name FROM t_mst_loc_district ld WHERE ld.id_district = ma.id_district)
+' '+
(SELECT lt.state_name FROM t_mst_loc_state lt WHERE lt.id_state = ma.id_state)
AS home_address
FROM t_mst_applicant ma
WHERE ma.id_applicant = ?");
  $stmt->execute(array($licNo));
  $data = $stmt->fetch(PDO::FETCH_OBJ);

  $licenseQuery = $mssql->prepare("SELECT * FROM (
SELECT ROW_NUMBER()  OVER (ORDER BY ml.created_date DESC) AS no_urut,
mk.lookup_value, ml.license_no 
FROM t_mst_license ml 
INNER JOIN t_mst_lookup mk
ON ml.id_license_type = mk.id_lookup
WHERE ml.id_applicant = ? ) RAMONES
WHERE no_urut < 2");
  $licenseQuery->execute(array($licNo));
  $dataLicenseQuery = $licenseQuery->fetch(PDO::FETCH_OBJ);

  $catRatingQuery = $mssql->prepare("SELECT
CASE WHEN
tl.aircraft_category_rating = 'Aeroplane'
THEN 'X' ELSE '' END AS isaerorplane,
CASE WHEN
tl.aircraft_category_rating = 'Rotocraft'
THEN 'X' ELSE '' END AS isrotocraft,
CASE WHEN
tl.aircraft_category_rating = 'Lighter Than Air'
THEN 'X' ELSE '' END AS islighterthanair,
CASE WHEN
tl.aircraft_category_rating = 'Glider'
THEN 'X' ELSE '' END AS isglider,
(SELECT
MAX(CASE WHEN
acr.id_class_rating = 'ACR11'
THEN CONVERT(VARCHAR(20),CONVERT(VARCHAR, acr.issued_date, 103))  ELSE '' END)
WHERE acr.id_mst_license = tl.id_mst_license) selissueddate,
(SELECT
MAX(
CASE WHEN
acr.id_class_rating = 'ACR12'
THEN CONVERT(VARCHAR(20),CONVERT(VARCHAR, acr.issued_date, 103)) ELSE '' END)
WHERE acr.id_mst_license = tl.id_mst_license) sesissueddate,
(SELECT
MAX(
CASE WHEN
acr.id_class_rating = 'ACR13'
THEN CONVERT(VARCHAR(20),CONVERT(varchar, acr.issued_date, 103)) ELSE '' END)
WHERE acr.id_mst_license = tl.id_mst_license) melissueddate,
(SELECT
MAX (
CASE WHEN
acr.id_class_rating = 'ACR14'
THEN CONVERT(VARCHAR(20),convert(varchar, acr.issued_date, 103)) ELSE '' END)
WHERE acr.id_mst_license = tl.id_mst_license) mesissueddate
FROM t_mst_license tl
LEFT JOIN t_mst_lic_aircraft_category_rating acr
ON tl.id_mst_license = acr.id_mst_license
WHERE tl.id_applicant = ?
AND tl.is_active = 1 AND acr.is_active = 1
GROUP BY tl.aircraft_category_rating,acr.id_mst_license,tl.id_mst_license");
  $catRatingQuery->execute(array($licNo));
  $dataCatRatingQuery = $catRatingQuery->fetch(PDO::FETCH_OBJ);

  unset($data->created_by);
  unset($data->created_date);
  unset($data->updated_by);
  unset($data->updated_date);

  if(!empty($data)) {
    $id_applicant = $data->id_applicant;

//    $stmt = $mssql->prepare("SELECT * FROM t_mst_applicant WHERE id_applicant = ?");
//    $stmt->execute(array($id_applicant));
//    $data_applicant = $stmt->fetch(PDO::FETCH_OBJ);
//    unset($data_applicant->created_by);
//    unset($data_applicant->created_date);
//    unset($data_applicant->updated_by);
//    unset($data_applicant->updated_date);
//
//    $stmt = $mssql->prepare("SELECT * FROM t_mst_organization WHERE id_org = ?");
//    $stmt->execute(array($data_applicant->id_org));
//    $data_organization = $stmt->fetch(PDO::FETCH_OBJ);
//    $data_applicant->id_org = $data_organization->org_name;

    $app->response->headers->set('Content-Type', 'application/json');
//    $response = array(
//      "data_applicant" => $data_applicant,
//      "data_license" => $data,
//      "data_photo_url" => "http://10.0.48.31/api/getApplicantPhoto/".$id_applicant,
//    );
    $response = array(
      "data_applicant" => $data,
      "data_photo_url" => "http://10.0.48.31/api/getApplicantPhoto/".$id_applicant,
      "data_license" => $dataLicenseQuery,
      "data_rating" => $dataCatRatingQuery,
    );
    echo json_encode($response);
//    $image = $data_applicant->photo_file;
//    $app->response->headers->set('Content-Type', 'image/jpeg');
//    echo $image;

  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});
$app->post('/api/category_class_rating', function() use ($app, $mssql) {
  $licNo = $_POST["lic_number"];

  $stmt = $mssql->prepare("SELECT 
CASE WHEN
tl.aircraft_category_rating = 'Aeroplane'
THEN 'X' ELSE '' END AS isaerorplane,
CASE WHEN
tl.aircraft_category_rating = 'Rotocraft'
THEN 'X' ELSE '' END AS isrotocraft,
CASE WHEN
tl.aircraft_category_rating = 'Lighter Than Air'
THEN 'X' ELSE '' END AS islighterthanair,
CASE WHEN
tl.aircraft_category_rating = 'Glider'
THEN 'X' ELSE '' END AS isglider,
(SELECT 
MAX(CASE WHEN
acr.id_class_rating IN ('ACR11','ACR21')
THEN CONVERT(VARCHAR(20),CONVERT(VARCHAR, acr.issued_date, 103))  ELSE '' END)
WHERE acr.id_mst_license = tl.id_mst_license) selissueddate,
(SELECT
MAX(
CASE WHEN
acr.id_class_rating IN ('ACR12','ACR22')
THEN CONVERT(VARCHAR(20),CONVERT(VARCHAR, acr.issued_date, 103)) ELSE '' END)
WHERE acr.id_mst_license = tl.id_mst_license) sesissueddate,
(SELECT
MAX(
CASE WHEN
acr.id_class_rating IN ('ACR13', 'ACR23')
THEN CONVERT(VARCHAR(20),CONVERT(varchar, acr.issued_date, 103)) ELSE '' END) 
WHERE acr.id_mst_license = tl.id_mst_license) melissueddate,
(SELECT
MAX (
CASE WHEN
acr.id_class_rating IN ('ACR14','ACR24')
THEN CONVERT(VARCHAR(20),convert(varchar, acr.issued_date, 103)) ELSE '' END) 
WHERE acr.id_mst_license = tl.id_mst_license) mesissueddate
FROM t_mst_license tl
LEFT JOIN t_mst_lic_aircraft_category_rating acr
ON tl.id_mst_license = acr.id_mst_license
WHERE tl.id_applicant = ?
AND tl.is_active = 1 AND acr.is_active = 1
GROUP BY tl.aircraft_category_rating,acr.id_mst_license,tl.id_mst_license");
  $stmt->execute(array($licNo));
  $data = $stmt->fetch(PDO::FETCH_OBJ);



  unset($data->created_by);
  unset($data->created_date);
  unset($data->updated_by);
  unset($data->updated_date);

  if(!empty($data)) {
    $app->response->headers->set('Content-Type', 'application/json');

    $response = array(
      "data_category_rating" => $data,
    );
    echo json_encode($response);


  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});

$app->post('/api/profile_personnel', function() use ($app, $mssql) {
  $licNo = $_POST["lic_number"];

  $stmt = $mssql->prepare("SELECT mu.id_applicant,
(SELECT ml.lookup_value FROM t_mst_lookup ml WHERE ml.lookup_group = 'license type'
AND ml.id_lookup = tl.id_license_type) as license,
tl.license_no AS lic_no,
CONVERT(varchar(20),convert(varchar, tl.lic_issued_date, 103)) AS date_of_issue_license, 
(SELECT mo.org_name FROM t_mst_organization mo WHERE mo.id_org = mu.id_org) AS org_name,
mu.fullname AS fullname,
mu.photo_file AS foto,
mu.address + ' RT:' + mu.address_rt + ' RW:' + mu.address_rw + ' ' +
(SELECT lv.village_name FROM t_mst_loc_village lv WHERE lv.id_village = mu.id_village) 
+ ' '+ 
(SELECT ls.subdist_name FROM t_mst_loc_subdistrict ls WHERE ls.id_subdist = mu.id_subdist)
+' '+
(SELECT ld.district_name FROM t_mst_loc_district ld WHERE ld.id_district = mu.id_district)
+' '+
(SELECT lt.state_name FROM t_mst_loc_state lt WHERE lt.id_state = mu.id_state)
AS home_address
,
UPPER(mu.place_of_birth) + ' '+CONVERT(varchar(20),convert(varchar, mu.birth_date, 103)) as placeandbirth,
mu.height,
mu.weight,
(SELECT ml.lookup_value FROM t_mst_lookup ml WHERE ml.lookup_group = 'hair color'
AND ml.id_lookup = mu.id_hair_color
) as hair_type_color,
(SELECT ml.lookup_value FROM t_mst_lookup ml WHERE ml.lookup_group = 'eye color'
AND ml.id_lookup = mu.id_eyes_color
) as eye_type_color,
(SELECT ml.lookup_value FROM t_mst_lookup ml WHERE ml.lookup_group = 'gender'
AND ml.id_lookup = mu.id_gender
) as sex_code,
(SELECT mlc.country_name
FROM t_mst_loc_country mlc
WHERE mlc.id_country = mu.id_nationality
) AS nationality,
CONVERT(VARCHAR(20),CONVERT(varchar, tl.lic_expired_date, 103)) AS expiration_date
FROM t_mst_license tl
INNER JOIN t_mst_applicant mu
ON tl.id_applicant = mu.id_applicant
WHERE tl.id_applicant = ?
ORDER BY license ASC");
  $stmt->execute(array($licNo));
  $data = $stmt->fetch(PDO::FETCH_OBJ);



  unset($data->created_by);
  unset($data->created_date);
  unset($data->updated_by);
  unset($data->updated_date);
  $data->photo_url = "http://10.0.48.31/api/getApplicantPhoto/".$data->id_applicant;



  if(!empty($data)) {
    $app->response->headers->set('Content-Type', 'application/json');

    $response = array(
      "data_profile_personnel" => $data,
//      "data_photo_url" => "http://10.0.48.31/api/getApplicantPhoto/".$data->id_applicant,
    );
    echo json_encode($response);


  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});
$app->post('/api/category_class_rating', function() use ($app, $mssql) {
  $licNo = $_POST["lic_number"];

  $stmt = $mssql->prepare("SELECT 
CASE WHEN
tl.aircraft_category_rating = 'Aeroplane'
THEN 'X' ELSE '' END AS isaerorplane,
CASE WHEN
tl.aircraft_category_rating = 'Rotocraft'
THEN 'X' ELSE '' END AS isrotocraft,
CASE WHEN
tl.aircraft_category_rating = 'Lighter Than Air'
THEN 'X' ELSE '' END AS islighterthanair,
CASE WHEN
tl.aircraft_category_rating = 'Glider'
THEN 'X' ELSE '' END AS isglider,
(SELECT 
MAX(CASE WHEN
acr.id_class_rating IN ('ACR11','ACR21')
THEN CONVERT(VARCHAR(20),CONVERT(VARCHAR, acr.issued_date, 103))  ELSE '' END)
WHERE acr.id_mst_license = tl.id_mst_license) selissueddate,
(SELECT
MAX(
CASE WHEN
acr.id_class_rating IN ('ACR12','ACR22')
THEN CONVERT(VARCHAR(20),CONVERT(VARCHAR, acr.issued_date, 103)) ELSE '' END)
WHERE acr.id_mst_license = tl.id_mst_license) sesissueddate,
(SELECT
MAX(
CASE WHEN
acr.id_class_rating IN ('ACR13', 'ACR23')
THEN CONVERT(VARCHAR(20),CONVERT(varchar, acr.issued_date, 103)) ELSE '' END) 
WHERE acr.id_mst_license = tl.id_mst_license) melissueddate,
(SELECT
MAX (
CASE WHEN
acr.id_class_rating IN ('ACR14','ACR24')
THEN CONVERT(VARCHAR(20),convert(varchar, acr.issued_date, 103)) ELSE '' END) 
WHERE acr.id_mst_license = tl.id_mst_license) mesissueddate
FROM t_mst_license tl
LEFT JOIN t_mst_lic_aircraft_category_rating acr
ON tl.id_mst_license = acr.id_mst_license
WHERE tl.id_applicant = ?
AND tl.is_active = 1 AND acr.is_active = 1
GROUP BY tl.aircraft_category_rating,acr.id_mst_license,tl.id_mst_license");
  $stmt->execute(array($licNo));
//  $data = $stmt->fetch(PDO::FETCH_OBJ);
  $data = $stmt->fetchAll();


  unset($data->created_by);
  unset($data->created_date);
  unset($data->updated_by);
  unset($data->updated_date);

  if(!empty($data)) {
    $app->response->headers->set('Content-Type', 'application/json');

    $response = array(
      "data_category_rating" => $data,
    );
    echo json_encode($response);


  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});
$app->post('/api/type_rating', function() use ($app, $mssql) {
  $licNo = $_POST["lic_number"];

  $stmt = $mssql->prepare("SELECT 
ROW_NUMBER()  OVER (ORDER BY atr.issued_date ASC) AS Row,
atr.type_rating_name AS type_rating,
convert(varchar, atr.issued_date , 103)  AS issued_date
FROM t_mst_license tl
LEFT JOIN t_mst_lic_aircraft_type_rating atr
ON tl.id_mst_license = atr.id_mst_license
WHERE tl.id_applicant = ?
AND tl.is_active = 1 AND atr.is_active = 1
GROUP BY tl.aircraft_category_rating,tl.id_mst_license,
atr.type_rating_name,atr.issued_date,tl.id_applicant");
  $stmt->execute(array($licNo));
//  $data = $stmt->fetch(PDO::FETCH_ASSOC);
  $data = $stmt->fetchAll();


//  unset($data->created_by);
//  unset($data->created_date);
//  unset($data->updated_by);
//  unset($data->updated_date);

  if(!empty($data)) {
    $app->response->headers->set('Content-Type', 'application/json');

    $response = array(
      "data_type_rating" => $data,
    );
    echo json_encode($response);


  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});
$app->post('/api/instrument_rating', function() use ($app, $mssql) {
  $licNo = $_POST["lic_number"];

  $stmt = $mssql->prepare("SELECT
	ROW_NUMBER()  OVER (ORDER BY ir.checked_date ASC) AS Row,
	CASE WHEN
	ir.id_air_sim_type = 'AST02'
	THEN 'X' ELSE '' END AS issim,
	CASE WHEN
	ir.id_air_sim_type = 'AST01'
	THEN 'X' ELSE '' END AS isaircraft,
	CONVERT(varchar(20),convert(varchar, ir.checked_date, 103)) as check_dt,
	CONVERT(varchar(20),convert(varchar, ir.expired_date, 103)) as expiry_dt
	FROM t_trx_lic_instrument_rating ir
	WHERE ir.id_trx_license in (
	SELECT ml.id_trx_license FROM t_trx_license ml WHERE
	ml.id_applicant = ?
	)
	AND GETDATE() <= ir.expired_date
	AND ir.is_active = 1
	GROUP BY ir.expired_date,ir.checked_date,ir.id_air_sim_type");
  $stmt->execute(array($licNo));
  $data = $stmt->fetchAll();



  unset($data->created_by);
  unset($data->created_date);
  unset($data->updated_by);
  unset($data->updated_date);

  if(!empty($data)) {
    $app->response->headers->set('Content-Type', 'application/json');

    $response = array(
      "data_instrument_rating" => $data,
    );
    echo json_encode($response);


  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});
$app->post('/api/ppc', function() use ($app, $mssql) {
  $licNo = $_POST["lic_number"];

  $stmt = $mssql->prepare("SELECT
ROW_NUMBER()  OVER (ORDER BY ppc.checked_date ASC) AS no_urut,
CASE WHEN ppc.ppc_type_rating= 'Single Engine Land' THEN 'SE LAND' ELSE
	CASE WHEN ppc.ppc_type_rating = 'Single Engine Sea' THEN 'SE SEA' ELSE
	CASE WHEN ppc.ppc_type_rating = 'Multi Engine Land' THEN 'ME LAND' ELSE
	CASE WHEN ppc.ppc_type_rating = 'Multi Engine Sea' THEN 'ME SEA'
	ELSE ppc.ppc_type_rating
	END
	END
	END
END AS type_rating,
CASE WHEN
ppc.id_air_sim_type = 'AST02'
THEN 'X' ELSE '' END AS issim,
CASE WHEN
ppc.id_air_sim_type = 'AST01'
THEN 'X' ELSE '' END AS isaircraft,
CONVERT(varchar(20),convert(varchar, ppc.checked_date, 103)) as check_dt,
CONVERT(varchar(20),convert(varchar, ppc.expired_date, 103)) as expiry_dt,
ppc.ppc_pic as pic
FROM t_trx_lic_ppc ppc
WHERE ppc.id_trx_license in (
SELECT ml.id_trx_license FROM t_trx_license ml WHERE
ml.id_applicant = ?
)
AND GETDATE() <= ppc.expired_date
AND ppc.is_active = 1
GROUP BY ppc.expired_date,ppc.ppc_type_rating,ppc.id_air_sim_type,ppc.checked_date,ppc.ppc_pic");
  $stmt->execute(array($licNo));
  $data = $stmt->fetchAll();



  unset($data->created_by);
  unset($data->created_date);
  unset($data->updated_by);
  unset($data->updated_date);

  if(!empty($data)) {
    $app->response->headers->set('Content-Type', 'application/json');

    $response = array(
      "data_ppc" => $data,
    );
    echo json_encode($response);


  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});
$app->post('/api/epc', function() use ($app, $mssql) {
  $licNo = $_POST["lic_number"];

  $stmt = $mssql->prepare("SELECT 
(
SELECT ma.radio_telephone_number 
FROM t_mst_applicant ma
WHERE ma.id_applicant = tl.id_applicant) AS radio_telp_no,
CASE WHEN
CONVERT(varchar(20),te.level) IS NULL
THEN '-'
WHEN
CONVERT(varchar(20),te.level) = '4'
THEN '4 (FOUR)'
WHEN
CONVERT(varchar(20),te.level) = '5'
THEN '5 (FIVE)'
WHEN
CONVERT(varchar(20),te.level) = '6'
THEN 'PERMANENT' ELSE
CONVERT(varchar(20),te.level) 
END AS lvl,
CASE WHEN
te.date_of_exam IS NULL
THEN '-' ELSE 
CONVERT(varchar(20),convert(varchar, te.date_of_exam, 103)) END AS test,
CASE WHEN
te.expiry_date IS NULL
THEN '-' ELSE 
CONVERT(varchar(20),convert(varchar, te.expiry_date, 103)) END AS expiry
FROM t_trx_lic_epc te
RIGHT JOIN t_trx_license tl
ON te.id_trx_license = tl.id_trx_license
WHERE tl.id_trx_license IN
(SELECT tl2.id_trx_license FROM t_trx_license tl2 WHERE tl2.id_applicant = ?)
ORDER BY te.expiry_date DESC");
  $stmt->execute(array($licNo));
  $data = $stmt->fetch(PDO::FETCH_OBJ);

  unset($data->created_by);
  unset($data->created_date);
  unset($data->updated_by);
  unset($data->updated_date);

  if(!empty($data)) {
    $app->response->headers->set('Content-Type', 'application/json');

    $response = array(
      "data_epc" => $data,
    );
    echo json_encode($response);


  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});
$app->get('/api/getApplicantPhoto/:idAppplicant', function($idApplicant) use ($app, $mssql) {

  $stmt_ts = $mssql->prepare("SET TEXTSIZE 10000000");
  $stmt_ts->execute();
  $stmt = $mssql->prepare("SELECT * FROM t_mst_applicant WHERE id_applicant = ?");
  $stmt->execute(array($idApplicant));
  $data_applicant = $stmt->fetch(PDO::FETCH_OBJ);



  if(!empty($data_applicant)) {

    $image = $data_applicant->photo_file;
//    $img = hex2bin($image);

    $app->response->headers->set('Content-Type', 'image/jpeg');
    echo base64_decode(chunk_split(base64_encode($image)));


  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});

// Mobile Dashboard HubPayment
// API
$app->get('/hubapi/dashboard', function() use ($app, $mssql) {

  $data = $app->db->table('Trx_Record')->whereRaw("req_time >= UNIX_TIMESTAMP(CURDATE())")->get();
  $tdyPaid = $app->db->table('Trx_Record')->whereRaw("req_time >= UNIX_TIMESTAMP(CURDATE()) AND req_paid = 1")->get();

  if(!empty($data)) {
    $app->response->headers->set('Content-Type', 'application/json');
    
    $total = null;
    $i = 0;
    foreach ($data as $d) {
      $reqentity = json_decode($d["req_entity"], true);
      $total+= $reqentity["data"]["PaymentHeader"]["TotalNominalBilling"];
      $i+=1;
    }
    $response = array(
      "today_request" => count($data),
      "today_paid" => count($tdyPaid),
      "today_income" => number_format($total),
      "data" => $data,
    );

    echo json_encode($response);

  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});

$app->get('/hubapi/paid', function() use ($app, $mssql) {

  $tdyPaid = $app->db->table('Trx_Record')->whereRaw("req_time >= UNIX_TIMESTAMP(CURDATE()) AND req_paid = 1")->get();

  if(!empty($tdyPaid)) {
    $app->response->headers->set('Content-Type', 'application/json');

    $total = null;
    $payload = array();
    $i = 0;
    foreach ($tdyPaid as $d) {
      $reqentity = json_decode($d["req_entity"], true);
      $payload[$i]["inv_no"] = $reqentity["invoiceNo"];
      $payload[$i]["airline"] = $reqentity["data"]["PaymentHeader"]["NamaWajibBayar"];
      $i+=1;
    }
    $response = array(
      "payload" => $payload,
    );

    echo json_encode($response);

  } else {
    $app->response->headers->set('Content-Type', 'application/json');
    $content = array("message" => "Not Found", "status" => 404);
    $response = array("response" => $content);
    echo json_encode($response);
  }

});
$app->run();

 ?>
