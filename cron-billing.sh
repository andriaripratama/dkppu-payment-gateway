#!/bin/bash

PIDS=`ps aux | grep cron-billing.php | grep -v grep`
if [ -z "$PIDS" ]; then
    php /home/app/cron-billing.php &
else
    echo "cron-billing.php already running."
fi
