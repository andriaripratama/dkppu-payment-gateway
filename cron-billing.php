<?php

$curl = curl_init();
curl_setopt ($curl, CURLOPT_URL, "http://localhost/billingstatusrequest");
curl_exec ($curl);
curl_close ($curl);
// require_once 'vendor/autoload.php';
// require 'app/init-cron.php';
//
//
// // Insert Log bahwa cron berjalan
// $log = $app->db->table("Log")->insertGetId([
//     "type" => "INFO_LOG_BILLING_CHECK", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s"), "create_at" => time()
// ]);
// // Inisiasi SOAP CLIENT
// $client = new SoapClient("http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl");
//
// // Mengambil data yang akan dikirim ke SOA HubPayment
// $stmt = $mssql->prepare("SELECT * FROM t_trx_license_invoice WHERE expired_date >= GETDATE();");
// $stmt->execute();
// $log = $app->db->table("Log")->insertGetId([
//     "type" => "INFO_LOG_BILLING_CHECK", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE MSSQL QUERY", "create_at" => time()
// ]);
//
// // Mengolah data yang diterima dari hasil query diatas
// while ($row = $stmt->fetch()) {
//   // Enkapsulasi data menjadi Array yang nantinya dikirimkan ke SOA HubPayment
//   $payment = array(
//   	"appsId" => "009",
//   	"invoiceNo" => $row["invoice_no"],
//   	"routeId" => "001",
//   	"TrxId" => "ANY-JK",
//   	"UserId" => "0",
//   	"Password" => "0",
//   	"KodeBillingSimponi" => $row["billing_code"],
//   	"KodeKL" => "022",
//   	"KodeEselon1" => "05",
//   	"KodeSatker" => "465632",
//   );
//   // Merubah array yang diatas menjadi format JSON
//   $paymentJson = json_encode($payment);
//   // Mengirim ke fungsi SOA HubPayment
//   $res = $client->BillingStatus($payment);
//   // Data yang diterima dari fungsi SOA HubPayment diconvert menjadi JSON
//   // $res = json_encode($res);
//   // Data yang tadi diterima sudah menjadi JSON lalu dirubah menjadi Array
//   $data = json_decode(json_encode($res), true);
//   $dataJson = json_encode($data);
//   $log = $app->db->table("Log")->insertGetId([
//       "type" => "INFO_LOG_BILLING_CHECK", "message" => "CRON RUNNING AT ".date("d-m-Y H:i:s").", SUCCESS EXECUTE SOAP FUNCTION AND DECODE", "create_at" => time()
//   ]);
//
//   // Jika response code sama dengan 00 ( sukses )
//   // echo $data["ResponseData"]["SimponiTrxId"];
//   // echo $data["ResponseCode"];
//   if($data["ResponseCode"] == "00") {
//     // Simpan data pada database MySQL local API Payment sebagai arsip
//     $log = $app->db->table('Trx_Record')->where('req_billingcode', $row["billing_code"])->update([
//       'req_paid' => 1
//     ]);
//     // Update data yang berada pada database MS SQL, memasukkan Billing Code dan merubah flag = 1
//
//     try {
//
//       $stmt = $mssql->prepare("UPDATE t_trx_license_invoice SET is_paid=? WHERE id_trx_lic_invoice=?");
//       $stmt->execute(array(1,$row["id_trx_lic_invoice"]));
//
//     } catch (PDOException $e) {
//       echo $e->getMessage();
//     }
//     // echo "Success Input Payment with ID ". $idtx .", Billing Code ". $kodeBilling;
//     // die();
//   } else {
//     // Jika response code bukan sama dengan 00 maka akan disimpan pada database MySQL untuk tindak lanjut
//     // $log = $app->db->table('Trx_Record')->insertGetId([
//     //   'req_time' => time(), 'req_entity' => $paymentJson, 'req_response_code' => $data["ResponseCode"], 'req_message' => $data["ResponseMessage"]
//     // ]);
//     // var_dump($data);
//   }
// }

?>
